﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace DataAccess.Entities
{
    public static class DbUtility
    {
        private static object _locker = new object();

        public static void CreateDatabase()
        {
            string dbPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "database.db3");
            var db = new SQLiteConnection(dbPath);
            db.CreateTable<Category>();
            db.CreateTable<Manufacturer>();
            db.CreateTable<Product>();
            db.CreateTable<Price>();
            db.CreateTable<BlacklistedShop>();
            if (!db.Table<Category>().Any())
            {
                var videocards = new Category();
                videocards.Name = "Видеокарты";
                videocards.Url = "http://catalog.onliner.by/videocard/";
                db.Insert(videocards);
                var audiocards = new Category();
                audiocards.Name = "Звуковые карты";
                audiocards.Url = "http://catalog.onliner.by/soundcard/";
                db.Insert(audiocards);
                var notebooks = new Category();
                notebooks.Name = "Ноутбуки";
                notebooks.Url = "http://catalog.onliner.by/notebook/";
                db.Insert(notebooks);
            }
        }

        public static List<Category> GetAllCategories()
        {
            string dbPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "database.db3");
            var db = new SQLiteConnection(dbPath);
            return db.Table<Category>().ToList();
        }

        public static void SaveCategories(List<Category> categories)
        {
            string dbPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "database.db3");
            var db = new SQLiteConnection(dbPath);
            db.DeleteAll<Category>();
            db.InsertAll(categories);
        }

        public static List<BlacklistedShop> GetAllBlacklisted()
        {
            string dbPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "database.db3");
            var db = new SQLiteConnection(dbPath);
            return db.Table<BlacklistedShop>().ToList();
        }

        public static void SaveBlacklisted(List<BlacklistedShop> blackListed)
        {
            string dbPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "database.db3");
            var db = new SQLiteConnection(dbPath);
            db.DeleteAll<BlacklistedShop>();
            db.InsertAll(blackListed);
        }

        public static int Save <T>( T item)
        {
            string dbPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "database.db3");
            var db = new SQLiteConnection(dbPath);
            lock (_locker)
            {
                return db.Insert(item);
            }
        }

        public static void ClearPreviousData()
        {
            string dbPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "database.db3");
            var db = new SQLiteConnection(dbPath);
            db.DeleteAll<Manufacturer>();
            db.DeleteAll<Product>();
            db.DeleteAll<Price>();
        }

        public static List<Price> GetMinimalPricesForCategotyProducts(Category category, List<BlacklistedShop> blackList)
        {
            var ignore = blackList.Select(s => string.Format("http://{0}.shop.onliner.by", s.ShopId)).ToList();
            var prices = new List<Price>();
            string dbPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "database.db3");
            var db = new SQLiteConnection(dbPath);
            var products = db.Table<Product>().Where(s => s.IsAvailable && s.CategoryId == category.Id);
            foreach (var product in products)
            {
                var pricesList =
                    db.Table<Price>()
                        .Where(s => s.ProductId == product.Id && s.IsInStock).ToList();
                var price = pricesList.Where(s => !ignore.Contains(s.ShopUrl))
                                  .OrderBy(m => m.TotalPrice)
                                  .FirstOrDefault();

                if (price == null)
                    continue;
                prices.Add(price);
            }
            return prices;
        }


    }
}
