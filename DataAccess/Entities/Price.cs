﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace DataAccess.Entities
{
    public class Price
    {
        [PrimaryKey]
        [AutoIncrement]
        public int Id { get; set; }
        public int Currency { get; set; }
        public int TotalPrice { get; set; }
        public string ShopUrl { get; set; }
        public bool IsInStock { get; set; }
        public bool IsCreditPossible { get; set; }
        public bool IsClearingPossible { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }
        public int CategoryId { get; set; }
    }
}
