﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace DataAccess.Entities
{
    public class Product
    {
        [PrimaryKey]
        [AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public bool IsAvailable { get; set; }
        public int ManufacturerId { get; set; }
        public int CategoryId { get; set; }

    }
}
