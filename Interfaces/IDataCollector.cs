﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Interfaces.ParsingItems;
using Ninject;

namespace Interfaces
{
    public interface IDataCollector
    {

        List<ManufacturerItem> GetAllManufacturersFromCategory(CategoryItem product);

        List<ProductItem> GetAllProductsFromManufacturer(ManufacturerItem product);

        List<PriceItem> GetAllPricesItemsFromProduct(ProductItem product, Currency currency);

        [Inject]
        IPageLoader PageLoader { get; set; }

    }

    public enum Currency
    {
        USD,
        BYR
    }
}
