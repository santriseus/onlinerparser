﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Ninject;

namespace Interfaces
{
    public interface IPageLoader
    {
        HtmlDocument Load (string url);
        [Inject]
        IProxyProvider ProxyProvider { get; set; }
    }
}
