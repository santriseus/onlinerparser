﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IProxyProvider
    {
        WebProxy GetNextProxy();
        void Success (WebProxy proxy);
        void Fail (WebProxy proxy);
    }
}
