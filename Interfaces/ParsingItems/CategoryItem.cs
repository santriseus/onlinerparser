﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces.ParsingItems
{
    public class CategoryItem
    {
        public CategoryItem(string name, string url)
        {
            Name = name;
            Url = url;
        }
        public string Name { get; private set; }
        public string Url { get; private set; }
        public override string ToString()
        {
            return String.Format("Category.Name:'{0}', Category.Url: '{1}'", Name, Url);
        }
    }
}
