﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces.ParsingItems
{
    public class ManufacturerItem
    {
        public ManufacturerItem(string name, string url, CategoryItem category)
        {
            Name = name;
            Url = url;
            Category = category;
        }
        public string Name { get; private set; }
        public string Url { get; private set; }
        public CategoryItem Category { get; private set; }
        public override string ToString()
        {
            return String.Format("Manufacturer.Name:'{0}', Manufacturer.Url:'{1}'", Name, Url, Category);
        }
    }
}
