﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces.ParsingItems;

namespace Interfaces
{
    public class PriceItem
    {
        public ProductItem Product { get; private set; }
        public int Price { get; private set; }
        public string ShopUrl { get; private set; }
        public Currency Currency { get; private set; }
        public bool IsInStock { get; private set; }
        public bool IsCreditPossible { get; private set; }
        public bool IsClearingPossible { get; private set; }
        public PriceItem (ProductItem product, int price, string url, Currency currency = Currency.USD, bool instock = true, bool isCreditPossible = true, bool isclearingPossible = true)
        {
            Price = price;
            ShopUrl = url;
            Currency = currency;
            IsInStock = instock;
            IsCreditPossible = isCreditPossible;
            IsClearingPossible = isclearingPossible;
        }

    }
}
