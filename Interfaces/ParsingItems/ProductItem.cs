﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces.ParsingItems
{
    public class ProductItem
    {
        public ProductItem(string name, string url, ManufacturerItem manufacturer, bool isAvailable = true)
        {
            Name = name;
            Url = url;
            IsAvailable = isAvailable;
        }
        public ManufacturerItem Manufacturer { get; private set; }
        public string Name { get; private set; }
        public string Url { get; private set; }
        public bool IsAvailable { get; private set; }

        public override string ToString()
        {
            return String.Format("Product.Name:'{0}', Product.Url: '{1}', Product.Available:'{2}'", Name, Url, IsAvailable.ToString());
        }
    }
}
