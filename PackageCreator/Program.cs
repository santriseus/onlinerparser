﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ionic.Zip;

namespace PackageCreator
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ZipFile zip = new ZipFile())
            {
                zip.CompressionLevel = Ionic.Zlib.CompressionLevel.BestCompression;
                zip.AddFile("chromedriver.exe");
                zip.AddFile("HtmlAgilityPack.dll");
                zip.AddFile("Interfaces.dll");
                zip.AddFile("Ionic.Zip.dll");
                zip.AddFile("Ninject.dll");
                zip.AddFile("NLog.dll");
                zip.AddFile("NLog.config");
                zip.AddFile("OnlinerParser.exe");
                zip.AddFile("OnlinerParser.exe.config");
                zip.AddFile("Parsing.Onliner.dll");
                zip.AddFile("ProductsForParsing.xml");
                zip.AddFile("WebDriver.dll");
                zip.AddFile("WebDriver.Support.dll");
                zip.Save("ConsoleParser_" + DateTime.Now.ToShortDateString() + ".zip");
            }
        }
    }
}
