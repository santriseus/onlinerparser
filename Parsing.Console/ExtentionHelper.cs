﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using OnlinerParser.Properties;
using NLog;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using System.Threading;

namespace OnlinerParser
{
    public static class ExtentionHelper
    {
        private static  object locker = new object();
        private static long RechargeTimeTicks = 0;



        public static bool LoadWithRetry(this HtmlAgilityPack.HtmlWeb web, ConcurrentQueue<Tuple<WebProxy, int>> proxies, string url, out HtmlDocument output)
        {
            try
            {
                if (DateTime.Now.Ticks < RechargeTimeTicks)
                {
                    LogManager.GetCurrentClassLogger().Info("Use proxy for loading because main IP is recharing.", url);
                    return LoadWithProxy(web, proxies, url, out output);
                }

                web.UseCookies = false;
                LogManager.GetCurrentClassLogger().Info("Loading url {0} without proxy.", url);
                web.PreRequest = delegate(HttpWebRequest webRequest)
                {
                    webRequest.Timeout = Settings.Default.NoProxyTimeout;
                    webRequest.ReadWriteTimeout = Settings.Default.NoProxyTimeout * 3;
                    return true;
                };
                output = web.Load(url);
                if (web.StatusCode != HttpStatusCode.OK)
                {
                    LogManager.GetCurrentClassLogger().Info("Status code is not OK: {0}", web.StatusCode.ToString());
                    Interlocked.Exchange(ref RechargeTimeTicks, DateTime.Now.AddSeconds(Settings.Default.RechargeTime).Ticks);
                    return LoadWithProxy(web, proxies, url, out output);
                }

                if (!output.DocumentNode.InnerText.Contains("onliner.by"))
                {
                    LogManager.GetCurrentClassLogger().Info("Loaded page does npt contain any 'onliner.by' string, probably it has not been loaded correctly: {0}", web.StatusCode.ToString());
                    Interlocked.Exchange(ref RechargeTimeTicks, DateTime.Now.AddSeconds(Settings.Default.RechargeTime).Ticks);
                    return LoadWithProxy(web, proxies, url, out output);
                }

                return true;
            }
            catch (Exception exception)
            {
                LogManager.GetCurrentClassLogger().InfoException("Error when loading page without proxy " + url, exception);
                Interlocked.Exchange(ref RechargeTimeTicks, DateTime.Now.AddSeconds(Settings.Default.RechargeTime).Ticks);
                return LoadWithProxy(web, proxies, url, out output);
            }
        }

        public static bool LoadWithProxy(this HtmlAgilityPack.HtmlWeb web, ConcurrentQueue<Tuple<WebProxy, int>> proxies, string url, out HtmlDocument output)
        {
            web.UseCookies = false;
            int numberOfRetry = 0;
            bool hasError = false;
            lock (locker)
            {
                if (proxies.Count < Settings.Default.NumberOfParrralelThreads * 2)
                {
                    foreach (var proxy in GetProxies())
                    {
                        proxies.Enqueue(proxy);
                    }
                }
            }
            do
            {
                Tuple<WebProxy, int> proxy;
                proxies.TryDequeue(out proxy);
                var numberOfErrors = proxy.Item2;
                try
                {
                    LogManager.GetCurrentClassLogger().Info("Loading url with proxy: {0}. Attempt {1}", url, numberOfRetry + 1);
                    web.PreRequest = delegate(HttpWebRequest webRequest)
                    {
                        webRequest.Timeout = Settings.Default.WithproxyTimeout;
                        webRequest.ReadWriteTimeout = Settings.Default.WithproxyTimeout * 3;
                        return true;
                    };
                    output = web.Load(url, "GET", proxy.Item1, new NetworkCredential());
                    if (web.StatusCode != HttpStatusCode.OK)
                    {
                        throw new Exception(String.Format("Status code is not OK: {0}", web.StatusCode.ToString()));
                    }
                    if (!output.DocumentNode.InnerText.Contains("onliner.by"))
                    {
                        throw new Exception(String.Format("Loaded page does npt contain any 'onliner.by' string, probably it has not been loaded correctly: {0}", web.StatusCode.ToString()));
                    }
                    hasError = false;
                }
                catch (Exception exception)
                {
                    LogManager.GetCurrentClassLogger().InfoException("Error when loading page " + url, exception);
                    output = new HtmlDocument();
                    hasError = true;
                    numberOfErrors++;
                }
                finally
                {
                    numberOfRetry++;
                    if (numberOfErrors < Settings.Default.NumberOfErrorsForProxy)
                    proxies.Enqueue(new Tuple<WebProxy, int>(proxy.Item1, numberOfErrors));
                }

            }
            while ((hasError || web.StatusCode != HttpStatusCode.OK) && numberOfRetry < Settings.Default.NumberOfRetries);

            return web.StatusCode == HttpStatusCode.OK;

        }

        public static ConcurrentQueue<Tuple<WebProxy, int>> GetProxies()
        {
            LogManager.GetCurrentClassLogger().Info("Getting proxies list.");
            var proxies = new ConcurrentQueue<Tuple<WebProxy, int>>();
            using (var driver = new ChromeDriver())
            {
                driver.Navigate().GoToUrl("http://spys.ru/en/http-proxy-list/");
                var numberOfResulrs = new SelectElement(driver.FindElementByCssSelector("#xpp"));
                numberOfResulrs.SelectByIndex(2);

                Thread.Sleep(3);
                while (driver.FindElementByCssSelector("#xf1") == null)
                {
                    Thread.Sleep(1);
                }
                var anon = new SelectElement(driver.FindElementByCssSelector("#xf1"));
                anon.SelectByIndex(1);


                //driver.Navigate().GoToUrl("http://spys.ru/en/http-proxy-list/");
                //var slowTime = driver.FindElementByCssSelector("#norightborder > fieldset:nth-child(1) > label:nth-child(2) > input:nth-child(1)");
                //slowTime.Click();
                //var slowConnection = driver.FindElementByCssSelector("#norightborder > fieldset:nth-child(2) > label:nth-child(2) > input:nth-child(1)");
                //slowConnection.Click();
                //var socksConnection = driver.FindElementByCssSelector(".proxyform > li:nth-child(3) > fieldset:nth-child(1) > label:nth-child(4) > input:nth-child(1)");
                //socksConnection.Click();

                //var button = driver.FindElementByCssSelector("#updateresults");
                //button.Submit();

                for (int i = 3; i < 100; i++)
                {
                    var ipSelector = String.Format("body > table:nth-child(3) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child({0}) > td:nth-child(1) > font.spy14", i);
                    //var portSelector = String.Format("#listtable > tbody:nth-child(2) > tr:nth-child({0}) > td:nth-child(3)", i);
                    var ip = driver.FindElementByCssSelector(ipSelector).Text;
                    if (ip.Split(':').Count() != 2)
                        continue;
                    var port = ip.Split(':')[1];
                    if (string.IsNullOrEmpty(ip) || string.IsNullOrEmpty(port))
                        continue;
                    int portNumber;
                    if (!int.TryParse(port, out portNumber) || portNumber < 0 || portNumber > 65535)
                        continue;
                    proxies.Enqueue(new Tuple<WebProxy, int>(new WebProxy(ip), 0));
                }

                driver.Close();
            }
            LogManager.GetCurrentClassLogger().Info("Extracted {0} proxies.", proxies.Count);
            return proxies;
        }
    }
}
