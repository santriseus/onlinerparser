﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlinerParser
{
    [Serializable]
    public class Product
    {
        public string Category { get; set; }
        public string Link { get; set; }
    }

    [Serializable]
    public class ProductsForProcessing
    {
        public List<Product> Products { get; set; }
    }
}
