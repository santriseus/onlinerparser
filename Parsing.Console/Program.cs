﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Serialization;
using HtmlAgilityPack;
using System.Linq;
using Interfaces;
using Ninject;
using NLog;
using OnlinerParser.Properties;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace OnlinerParser
{
    class Program
    {
        private static string catalogLink = "http://catalog.onliner.by/";

        public static ConcurrentQueue<Tuple<WebProxy, int>> Proxies;

        private static Logger _logger = LogManager.GetCurrentClassLogger();

        private static IKernel _kernel;



        static void Main(string[] args)
        {
            try
            {
                if (Directory.Exists("Results"))
                {
                    Directory.Delete("Results", true);
                }

                Directory.CreateDirectory("Results");

                if (Directory.Exists("Logs"))
                {
                    Directory.Delete("Logs", true);
                }

                _kernel = new StandardKernel();
                _kernel.Load("Parsing.Onliner.dll");


                var ser = new XmlSerializer(typeof(ProductsForProcessing));
                var products = (ProductsForProcessing)ser.Deserialize(new StreamReader("ProductsForParsing.xml"));

                var concurrentProducts = new ConcurrentQueue<Product>();

                Proxies = ExtentionHelper.GetProxies();

                foreach (var product in products.Products)
                {
                    concurrentProducts.Enqueue(product);
                }
                
                var productsTasks = new List<Task>();

                for (int i = 0; i < Settings.Default.NumberOfParrralelThreads; i++)
                {
                    var task = Task.Factory.StartNew(() =>
                    {
                        Product product;
                        while (concurrentProducts.TryDequeue(out product))
                        {
                            try
                            {
                                ProcessItem(product.Link, product.Category);
                            }
                            catch (Exception ex)
                            {
                                _logger.ErrorException(String.Format("Error occured when processing {0} in Category {1}", product.Link, product.Category), ex);
                            }
                        }

                    });
                   productsTasks.Add(task);
                }
                Task.WaitAll(productsTasks.ToArray());

                _logger.Info("Merging separate files to one.");

                foreach (var product in products.Products)
                {
                    var filename = Path.Combine("Results", String.Format("{0}.csv", product.Category));
                    if (File.Exists(filename))
                    {
                        _logger.Info("Processing file {0}", filename);
                        var lines = new List<string>();
                        using (var reader = new StreamReader(filename, Encoding.GetEncoding("windows-1251")))
                        {
                            while (!reader.EndOfStream)
                                lines.Add(reader.ReadLine());
                        }
                        using (var writer = new StreamWriter(Path.Combine("Results", "result.csv"), true, Encoding.GetEncoding("windows-1251")))
                        {
                            foreach (var line in lines)
                            {
                                writer.WriteLine(line);
                            }
                        }
                    }
                    else
                    {
                        _logger.Warn("File {0} for category {1} was not found in results", filename, product.Category);
                    }
                }

                _logger.Info("SUCESSFULLY FINISHED!");
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                _logger.ErrorException("Global Error occured:", ex);
            }
        }

        private static void ProcessItem( string link, string category)
        {
            HtmlDocument document = new HtmlDocument();
            var CyrylicWeb = new HtmlWeb
            {
                AutoDetectEncoding = false,
                OverrideEncoding = Encoding.GetEncoding("windows-1251")
            };
            var UnicodeWeb = new HtmlWeb
            {
                AutoDetectEncoding = false,
                OverrideEncoding = Encoding.UTF8
            };

            _logger.Info("Loading url: {0}", link);
            document = new HtmlDocument();
            if (!CyrylicWeb.LoadWithRetry(Proxies, link, out document))
            {
                _logger.Warn("Page {0} has not been loaded correcty. Skipping this item.", link);
                return;
            }
            _logger.Info("Loaded url: {0}", link);
            var manufacturers = CollectManufacturers(document);
            foreach (var manufacturer in manufacturers)
            {
                var manufacturerUrl = catalogLink + manufacturer.Value;
                int lastPage;
                string pageLink;
                if (GetPagesFromManufacturer(manufacturerUrl, CyrylicWeb, out lastPage, out pageLink)) continue;
                for (int i = 1; i < lastPage + 1; i++)
                {
                    var navigationLink = lastPage == 1 ? manufacturerUrl : catalogLink + pageLink.Substring(0, pageLink.IndexOf("page=")) + "page=" + i;
                    List<Tuple<string, string>> linskForPrices;

                    if (CollectLinks(navigationLink, CyrylicWeb, out linskForPrices)) continue;


                    var resultStrings = new List<string>();
                    foreach (var linkForPrice in linskForPrices)
                    {
                        GetResultPrice(category, linkForPrice, UnicodeWeb, resultStrings, manufacturer);
                    }
                    using (var writer = new StreamWriter(Path.Combine("Results", string.Format("{0}.csv", category)), true, Encoding.GetEncoding("windows-1251")))
                    {
                        foreach (var resultString in resultStrings)
                        {
                            writer.WriteLine(resultString);
                        }
                    }
                }
            }
        }

        private static bool CollectLinks(string navigationLink, HtmlWeb CyrylicWeb, out List<Tuple<string, string>> linskForPrices, bool isRetry = false)
        {
            linskForPrices = new List<Tuple<string, string>>();
            try
            {
                var pricePage = new HtmlDocument();
                _logger.Info("Loading url: {0}", navigationLink);
                if (!CyrylicWeb.LoadWithRetry(Proxies, navigationLink, out pricePage))
                {
                    _logger.Warn("Page {0} has not been loaded correcty. Skipping this item.", navigationLink);
                    return true;
                }
                _logger.Info("Loaded url: {0}", navigationLink);
                linskForPrices = CollectLinksForPrices(pricePage);
                return false;
            }
            catch (Exception ex )
            {
                _logger.WarnException("Error occured : ", ex);
                if (!isRetry)
                {
                    _logger.Info("Performing retry");
                    return CollectLinks(navigationLink, CyrylicWeb, out linskForPrices, true);
                }
                return false;
            }
        }

        private static bool GetPagesFromManufacturer(string manufacturerUrl, HtmlWeb CyrylicWeb, out int lastPage,
            out string pageLink, bool isRetry = false)
        {
            lastPage = 0;
            pageLink = null;
            try
            {
                _logger.Info("Loading url: {0}", manufacturerUrl);
                var manufacturerDocument = new HtmlDocument();
                if (!CyrylicWeb.LoadWithRetry(Proxies, manufacturerUrl, out manufacturerDocument))
                {
                    _logger.Warn("Page {0} has not been loaded correcty. Skipping this item.", manufacturerUrl);
                    return true;
                }
                _logger.Info("Loaded url: {0}", manufacturerUrl);
                lastPage = GetLastPageNumber(manufacturerDocument);
                pageLink = GetUrlExampleForPaging(manufacturerDocument);
                return false;
            }
            catch (Exception exception)
            {
                _logger.WarnException("Exception occured.", exception);
                if (!isRetry)
                {
                    _logger.Info("Performing retry");
                    return GetPagesFromManufacturer(manufacturerUrl, CyrylicWeb, out lastPage, out pageLink, true);
                }
                return false;
            }
        }

        private static void GetResultPrice(string category, Tuple<string, string> linkForPrice, HtmlWeb UnicodeWeb, List<string> resultStrings,
            KeyValuePair<string, string> manufacturer, bool isRetry = false)
        {
            try
            {
                var finder = _kernel.Get<IMinimalPriceFinder>();

                _logger.Info("Loading url: {0}", catalogLink + linkForPrice.Item1.Trim('/'));
                var pricesForItemPage = new HtmlDocument();
                if (
                    !UnicodeWeb.LoadWithRetry(Proxies, catalogLink + linkForPrice.Item1.Trim('/'), out pricesForItemPage))
                {
                    _logger.Warn("Page {0} has not been loaded correcty. Skipping this item.",
                        catalogLink + linkForPrice.Item1.Trim('/'));
                    return;
                }
                _logger.Info("Loaded url: {0}", catalogLink + linkForPrice.Item1.Trim('/'));
                if (!finder.HasInstockItems(pricesForItemPage))
                {
                    _logger.Info("Product {0} has shops, but is 'out of stock' everywhere.");
                    return;
                }
                var price = finder.GetMinimalPriceInStock(pricesForItemPage, Currency.USD);
                var result = new Tuple<int, string>(price.Price, price.ShopUrl);
                _logger.Info("Minimal price for product {0} is {1}. It was found in shop {2}.", linkForPrice.Item2,
                    result.Item1, result.Item2);
                resultStrings.Add(string.Format("{0};{1};{2};{3};{4}",
                    Encoding.Default.GetString(Encoding.Convert(Encoding.Default, Encoding.GetEncoding("windows-1251"),
                        Encoding.Default.GetBytes(category))),
                    HttpUtility.HtmlDecode(manufacturer.Key),
                    HttpUtility.HtmlDecode(new Regex(manufacturer.Key).Replace(linkForPrice.Item2, "", 1).Trim()),
                    result.Item1,
                    result.Item2
                    ));
            }
            catch (Exception exception)
            {
                _logger.WarnException("Exception occured.", exception);
                if (!isRetry)
                {
                    _logger.Info("Performing retry.");
                    GetResultPrice(category, linkForPrice, UnicodeWeb, resultStrings, manufacturer, true);
                }
            }

        }

        private static Dictionary<string, string> CollectManufacturers(HtmlDocument document)
        {
            _logger.Info("Collecting manufacturer links.");
            var manufacturers = new Dictionary<string, string>();
            try
            {
                HtmlNodeCollection links = document.DocumentNode.SelectNodes("//ul[@class='menuleft']/li/a");
                foreach (var link in links)
                {
                    manufacturers.Add(link.InnerText, link.GetAttributeValue("href", ""));
                }
                _logger.Info("Collected {0} manufacturer links.", manufacturers.Count);
            }
            catch (Exception ex)
            {
                _logger.ErrorException("Error occured : ", ex);
            }
            return manufacturers;
        }

        private static string GetUrlExampleForPaging(HtmlDocument document)
        {
            var link = document.DocumentNode.SelectSingleNode("//table[@class='phed']/tr/td/strong/a");
            if (link != null)
                return link.GetAttributeValue("href", "");
            return null;
        }

        private static int GetLastPageNumber(HtmlDocument document)
        {
            _logger.Info("Detecting last page number.");
            int someInt = 0;
            var links = document.DocumentNode.SelectNodes("//table[@class='phed']/tr/td/strong/a");
            if (links == null || !links.Any())
                return 1;
            var pageNmbers = links.Where(s => int.TryParse(s.InnerText, out someInt)).ToList();
            if (pageNmbers == null || !pageNmbers.Any())
                return 1;
            var number = pageNmbers.Max(s => int.Parse(s.InnerText));
            _logger.Info("Last page number is {0}.", number);
            return number;
        }

        private static List<Tuple<string,string>> CollectLinksForPrices(HtmlDocument document)
        {
            int ignored = 0;
            _logger.Info("Collecting links to product sellers list");
            var prices = new List<Tuple<string, string>>();
            try
            {
                HtmlNodeCollection nodes = document.DocumentNode.SelectNodes("//td[@class='poffers']/a");
                if (nodes == null)
                    return CollectLinksForPricesWithSubModels(document);

                foreach (var node in nodes)
                {
                    var link = node.GetAttributeValue("href", "");
                    if (!link.StartsWith("/informer"))
                    {
                        var nameNode = node.ParentNode.ParentNode.SelectSingleNode("td[@class='pdescr']/strong[@class='pname']/a");
                        var name = nameNode.InnerText.Trim('\n').Trim().Trim('\n').Trim().Trim('\n').Trim().Trim('\n').Trim();
                        prices.Add(new Tuple<string, string>(link, name));
                    }
                    else
                    {
                        ignored++;
                    }
                }
                _logger.Info("Links to product sellers list where collected for {0} products. {1} products are not avaliable at shops now.", prices.Count, ignored);
            }
            catch (Exception ex)
            {
                _logger.ErrorException("Error occured : ", ex);
            }
     
            return prices;
        }

        private static List<Tuple<string, string>> CollectLinksForPricesWithSubModels(HtmlDocument document)
        {
            int ignored = 0;
            _logger.Info("Collecting links to product sellers list with sub modules.");
            var prices = new List<Tuple<string, string>>();
            try
            {
                HtmlNodeCollection links = document.DocumentNode.SelectNodes("//td[@class='pprice']/a");
                foreach (var link in links)
                {
                    var href = link.GetAttributeValue("href", null);
                    var nameNode = link.ParentNode.ParentNode.SelectSingleNode("td[@class='pdescr']/strong[@class='pname']/a");
                    var name = nameNode.InnerText.Trim('\n').Trim().Trim('\n').Trim().Trim('\n').Trim().Trim('\n').Trim();
                    prices.Add(new Tuple<string, string>(href, name));
                }
                _logger.Info("Links to product sellers list where collected for {0} products. ", prices.Count);
            }
            catch (Exception ex)
            {
                _logger.ErrorException("Error occured : ", ex);
            }

            return prices;
        }

        private static Tuple<int, string> GetMinimalPrice(HtmlDocument document)
        {
            _logger.Info("Looking for minimal price.");
            var instock = document.DocumentNode.SelectNodes("//table[@class='b-offers-list-line-table__table']/tbody[@class='js-position-wrapper']/tr/td[2]/p[1]/span[1]").Where(s=>s.InnerText.Contains("В наличии"));
            var olo = instock.Select(s => s.ParentNode.ParentNode.ParentNode.SelectSingleNode("td[1]/p[1]/a")).ToList();
            var minimal = instock.Min(s => int.Parse(s.ParentNode.ParentNode.ParentNode.SelectSingleNode("td[1]/p[1]/a").InnerText.Replace("у.е.", "").Trim().Replace(" ","")));
            var minimalNode = instock.First(s => int.Parse(s.ParentNode.ParentNode.ParentNode.SelectSingleNode("td[1]/p[1]/a").InnerText.Replace("у.е.", "").Trim().Replace(" ", "")) == minimal);
            var link = minimalNode.ParentNode.ParentNode.ParentNode.SelectSingleNode("td[1]/p[1]/a");
            var prices = new Tuple<int, string>(int.Parse(link.InnerText.Replace("у.е.", "").Trim().Replace(" ", "")), link.GetAttributeValue("href", ""));
            return prices;
        }

        private static bool HasInstockItems (HtmlDocument document)
        {
            var items = document.DocumentNode.SelectNodes("//table[@class='b-offers-list-line-table__table']/tbody[@class='js-position-wrapper']/tr/td[2]/p[1]/span[1]").ToList();
            if (items == null || !items.Any())
                return false;
            var instock = items.Where(s => s.InnerText.Contains("В наличии"));
            
            var count = instock.Count();
            _logger.Info("{0} sellers have product in stock.", count);
            return count > 0;
        }

    }
}
