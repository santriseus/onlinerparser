﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Interfaces;
using Interfaces.ParsingItems;
using Ninject;
using NLog;

namespace Parsing.Onliner
{
    public class OnlinerDataCollector : IDataCollector
    {
        #region private

        private bool _useProxy = false;

        [Inject]
        public IPageLoader PageLoader { get; set; }

        private const string PiceItemsXPath = "//div[@id='region-minsk']/div[@class='b-offers-list-line-table']/table[@class='b-offers-list-line-table__table']/tbody[@class='js-position-wrapper']/tr/td[2]/p[1]/span[1]";

        private Logger _logger = LogManager.GetCurrentClassLogger();

        private HtmlDocument GetDocumentFromProduct(ProductItem product)
        {
            return PageLoader.Load(product.Url);
        }

        private HtmlDocument GetDocumentFromManufacturer(ManufacturerItem manufacturer)
        {
            return PageLoader.Load(manufacturer.Url);
        }

        private HtmlDocument GetDocumentFromCategory(CategoryItem category)
        {
            return PageLoader.Load(category.Url);
        }

        private HtmlDocument GetDocumentFromDirectLink(string link)
        {
            return PageLoader.Load(link);
        }

        private List<PriceItem> GetAllPricesItemsFromPage(ProductItem product, HtmlDocument document, Currency currency)
        {
            _logger.Info("Getting prices for product.");

            var prices = new List<PriceItem>();

            var items = document.DocumentNode.SelectNodes(PiceItemsXPath);
            if (items == null || !items.Any())
                return prices;
            foreach (var item in items)
            {
                var inStock = item.InnerText.Contains("В наличии");
                var credit = item.ParentNode.InnerText.Contains("Возможно в кредит");
                var clearing = item.ParentNode.InnerText.Contains("Возможен безналичный расчет");
                var price = int.Parse(Regex.Replace(item.ParentNode.ParentNode.ParentNode.SelectSingleNode("td[1]/p[1]/a").InnerText, @"[^\d]", String.Empty));
                var shopUrl = item.ParentNode.ParentNode.ParentNode.SelectSingleNode("td[1]/p[1]/a").GetAttributeValue("href", "");
                prices.Add(new PriceItem(product, price, shopUrl, currency, inStock, credit, clearing));
            }
            _logger.Info("{0} prices where found. In stock :{1}. Clearing: {2}. Credit: {3}", prices.Count, prices.Count(s => s.IsInStock), prices.Count(s => s.IsClearingPossible), prices.Count(s => s.IsCreditPossible));
            return prices;
        }

        private List<string> GetLinksOfAllPages(HtmlDocument document, string baseUrl)
        {

            var links = new List<string>();
            var lastPage = GetLastPageNumber(document);
            for (int i = 1; i < lastPage + 1; i++)
            {
                links.Add(new Regex("~page=2").Replace(baseUrl, "~page=" + i, 1));
            }
            return links;
        }

        private int GetLastPageNumber(HtmlDocument document)
        {
            int someInt = 0;
            var links = document.DocumentNode.SelectNodes("//table[@class='phed']/tr/td/strong/a");
            if (links == null || !links.Any())
                return 1;
            var pageNmbers = links.Where(s => int.TryParse(s.InnerText, out someInt)).ToList();
            if (pageNmbers == null || !pageNmbers.Any())
                return 1;
            var number = pageNmbers.Max(s => int.Parse(s.InnerText));
            return number;
        }

        private List<ProductItem> GetAllProductsFromPage(HtmlDocument document, ManufacturerItem manufacturer)
        {
            _logger.Info("Collecting products from manufacturer : {0}.", manufacturer);
            List<ProductItem> products = new List<ProductItem>();
            HtmlNodeCollection nodes = document.DocumentNode.SelectNodes("//td[@class='poffers']/a");
            if (nodes == null) return GetAllNestedProductsFromPage(document, manufacturer);

            foreach (var node in nodes)
            {
                var link = node.GetAttributeValue("href", "");
                var nameNode = node.ParentNode.ParentNode.SelectSingleNode("td[@class='pdescr']/strong[@class='pname']/a");
                var name = nameNode.InnerText.Trim('\n').Trim().Trim('\n').Trim().Trim('\n').Trim().Trim('\n').Trim();
                var product = new ProductItem(name, "http://catalog.onliner.by" + link, manufacturer, !link.Contains("/informer"));
                products.Add(product);
                _logger.Info("Adding product: {0}.", product);

            }

            return products;
        }

        private List<ProductItem> GetAllNestedProductsFromPage(HtmlDocument document, ManufacturerItem manufacturer)
        {
            _logger.Info("Collecting sub-products from manufacturer : {0}.", manufacturer);
            var products = new List<ProductItem>();
            HtmlNodeCollection links = document.DocumentNode.SelectNodes("//td[@class='pprice']/a");
            foreach (var link in links)
            {
                var href = link.GetAttributeValue("href", null);
                var nameNode = link.ParentNode.ParentNode.SelectSingleNode("td[@class='pdescr']/strong[@class='pname']/a");
                var name = nameNode.InnerText.Trim('\n').Trim().Trim('\n').Trim().Trim('\n').Trim().Trim('\n').Trim();
                var product = new ProductItem(name, "http://catalog.onliner.by" + href, manufacturer, !href.StartsWith("/informer"));
                products.Add(product);
                _logger.Info("Adding sub-product: {0}.", product);
            }
            return products;
        }

        private List<ManufacturerItem> GetAllManufacturersFromPage(HtmlDocument document, CategoryItem category)
        {
            _logger.Info("Collecting manufacturers from category : {0}.", category);
            List<ManufacturerItem> manufacturers = new List<ManufacturerItem>();
            try
            {
                HtmlNodeCollection links = document.DocumentNode.SelectNodes("//ul[@class='menuleft']/li/a");
                foreach (var link in links)
                {
                    var manufacturer = new ManufacturerItem(link.InnerText, "http://catalog.onliner.by/" + link.GetAttributeValue("href", ""), category);
                    manufacturers.Add(manufacturer);
                    _logger.Info("Adding manufacturer: {0}.", manufacturer);
                }
                _logger.Info("Collected {0} manufacturers for category {1}.", manufacturers.Count, category);
            }
            catch (Exception ex)
            {
                _logger.ErrorException("Error occured : ", ex);
            }

            return manufacturers;
        }

        #endregion

        #region IMinimalPriceFinder

        public List<ManufacturerItem> GetAllManufacturersFromCategory(CategoryItem category)
        {
            return GetAllManufacturersFromPage(GetDocumentFromCategory(category), category);
        }

        public List<ProductItem> GetAllProductsFromManufacturer(ManufacturerItem manufacturer)
        {
            var products = new List<ProductItem>();

            var document = GetDocumentFromManufacturer(manufacturer);
            var baseUrl = GetBaseUrlForPaging(document);

            if (!string.IsNullOrEmpty(baseUrl))
            {
                List<string> pages = GetLinksOfAllPages(document, "http://catalog.onliner.by/" + baseUrl);
                if (pages.Count() > 1)
                {
                    foreach (var page in pages)
                    {
                        document = GetDocumentFromDirectLink(page);
                        products.AddRange(GetAllProductsFromPage(document, manufacturer));
                    }
                    return products;
                }
            }

            return GetAllProductsFromPage(document, manufacturer);
        }


        private string GetBaseUrlForPaging(HtmlDocument document)
        {
            var link = document.DocumentNode.SelectSingleNode("//table[@class='phed']/tr/td/strong/a");
            if (link != null)
                return link.GetAttributeValue("href", "");
            return null;
        }

        public List<PriceItem> GetAllPricesItemsFromProduct(Interfaces.ParsingItems.ProductItem product, Currency currency)
        {
            return GetAllPricesItemsFromPage(product, GetDocumentFromProduct(product), currency);
        }

        #endregion
    }
}
