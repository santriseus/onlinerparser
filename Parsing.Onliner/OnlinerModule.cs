﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;
using Ninject.Modules;

namespace Parsing.Onliner
{
    public class OnlinerModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IDataCollector>().To<OnlinerDataCollector>();
            Bind<IPageLoader>().To<SimplePageLoader>();
        }
    }
}
