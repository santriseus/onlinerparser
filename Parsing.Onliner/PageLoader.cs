﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Interfaces;
using Ninject;
using NLog;

namespace Parsing.Onliner
{
    internal class SimplePageLoader : IPageLoader
    {
        [Inject]
        public IProxyProvider ProxyProvider { get; set; }

        private HtmlWeb web = new HtmlWeb();

        private readonly int NoProxyTimeout = 3000;

        private readonly int WithproxyTimeout = 10000;

        private readonly int ProxyRetries = 20;

        private readonly int RechargeTime = 60;

        private long RechargeTimeTicks = 0;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public HtmlDocument Load(string url)
        {
            HtmlDocument document = null;

            try
            {
                Logger.Debug("Loading {0} without proxy.", url);
                document = LoadWithoutProxy(url);
                Logger.Debug("Sucessfully loaded {0} without proxy.", url);

            }
            catch (Exception noProxyEx)
            {
                if (!(noProxyEx is MainIpIsRechargingException))
                {
                    Interlocked.Exchange(ref RechargeTimeTicks, DateTime.Now.AddSeconds(RechargeTime).Ticks);
                    Logger.DebugException("Exception when loading without proxy.", noProxyEx);
                }
                if (ProxyProvider == null)
                    return null;
                var retriesLeft = ProxyRetries;
                while (retriesLeft > 0)
                {
                    try
                    {
                        Logger.Debug("Loading {0} with proxy. Retries left: {1}.", url, retriesLeft);
                        document = LoadWithProxy(url);
                        Logger.Debug("Sucessfully loaded {0} with proxy.", url);
                        return document;
                    }
                    catch (Exception proxyEx)
                    {
                        Logger.DebugException("Exception when loading with proxy.", proxyEx);
                        retriesLeft--;
                    }
                }
            }

            return document;
        }

        private HtmlDocument LoadWithoutProxy(string url)
        {
            HtmlDocument document = null;

            if (DateTime.Now.Ticks < RechargeTimeTicks)
            {
                Logger.Debug("MainIpIsRecharging.");
                throw new MainIpIsRechargingException();
            }

            web.PreRequest = delegate(HttpWebRequest webRequest)
            {
                webRequest.Timeout = NoProxyTimeout;
                webRequest.ReadWriteTimeout = NoProxyTimeout * 3;
                return true;
            };
            
            document = web.Load(url);

            if (web.StatusCode != HttpStatusCode.OK)
            {
                throw new StatusIsNotOkException();
            }

            if (!document.DocumentNode.InnerText.Contains("onliner.by"))
            {
                throw new InvalidPageException();
            }

            return document;
        }

        private HtmlDocument LoadWithProxy(string url)
        {
            HtmlDocument document = null;

            web.PreRequest = delegate(HttpWebRequest webRequest)
            {
                webRequest.Timeout = WithproxyTimeout;
                webRequest.ReadWriteTimeout = WithproxyTimeout * 3;
                return true;
            };

            var proxy = ProxyProvider.GetNextProxy();

            document = web.Load(url, "GET", proxy, new NetworkCredential());

            if (web.StatusCode != HttpStatusCode.OK)
            {
                ProxyProvider.Fail(proxy);
                throw new StatusIsNotOkException();
            }

            if (!document.DocumentNode.InnerText.Contains("onliner.by"))
            {
                ProxyProvider.Fail(proxy);
                throw new InvalidPageException();
            }
            ProxyProvider.Success(proxy);
            return document;
        }

    }
}
