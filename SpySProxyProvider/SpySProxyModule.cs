﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;
using Ninject.Modules;

namespace SpySProxyProvider
{
    public class SpySProxyModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IProxyProvider>().To<SpySProxyProvider>().InSingletonScope();
        }
    }
}
