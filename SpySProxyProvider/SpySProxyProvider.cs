﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using Interfaces;
using NLog;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SpySProxyProvider
{
    public class SpySProxyProvider : IProxyProvider
    {
        private static readonly int MaximumErrorsRate = 0;
        
        private static readonly object _locker = new object();

        private static readonly SpySProxyProvider _instance = new SpySProxyProvider();

        private static ConcurrentQueue<WebProxy> _proxies = new ConcurrentQueue<WebProxy>(); 

        private static Dictionary<string, int> _errors = new Dictionary<string, int>();

        private static void CollectProxies()
        {
            LogManager.GetCurrentClassLogger().Debug("Getting proxies list.");

            using (var driver = new ChromeDriver())
            {
                driver.Navigate().GoToUrl("http://spys.ru/en/http-proxy-list/");
                while (driver.FindElementByCssSelector("#xpp") == null)
                {
                    Thread.Sleep(1000);
                }
                var numberOfResulrs = new SelectElement(driver.FindElementByCssSelector("#xpp"));
                numberOfResulrs.SelectByIndex(2);

                Thread.Sleep(3000);
                while (driver.FindElementByCssSelector("#xf1") == null)
                {
                    Thread.Sleep(1000);
                }
                var anon = new SelectElement(driver.FindElementByCssSelector("#xf1"));
                anon.SelectByIndex(1);


                //driver.Navigate().GoToUrl("http://spys.ru/en/http-proxy-list/");
                //var slowTime = driver.FindElementByCssSelector("#norightborder > fieldset:nth-child(1) > label:nth-child(2) > input:nth-child(1)");
                //slowTime.Click();
                //var slowConnection = driver.FindElementByCssSelector("#norightborder > fieldset:nth-child(2) > label:nth-child(2) > input:nth-child(1)");
                //slowConnection.Click();
                //var socksConnection = driver.FindElementByCssSelector(".proxyform > li:nth-child(3) > fieldset:nth-child(1) > label:nth-child(4) > input:nth-child(1)");
                //socksConnection.Click();

                //var button = driver.FindElementByCssSelector("#updateresults");
                //button.Submit();

                for (int i = 3; i < 100; i++)
                {
                    var ipSelector = String.Format("body > table:nth-child(3) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child({0}) > td:nth-child(1) > font.spy14", i);
                    //var portSelector = String.Format("#listtable > tbody:nth-child(2) > tr:nth-child({0}) > td:nth-child(3)", i);
                    while (driver.FindElementByCssSelector(ipSelector) == null)
                    {
                        Thread.Sleep(1000);
                    }
                    var ip = driver.FindElementByCssSelector(ipSelector).Text;
                    if (ip.Split(':').Count() != 2)
                        continue;
                    var port = ip.Split(':')[1];
                    if (string.IsNullOrEmpty(ip) || string.IsNullOrEmpty(port))
                        continue;
                    int portNumber;
                    if (!int.TryParse(port, out portNumber) || portNumber < 0 || portNumber > 65535)
                        continue;
                    _proxies.Enqueue(new WebProxy(ip));
                }

                driver.Close();
            }
            LogManager.GetCurrentClassLogger().Debug("Extracted {0} proxies.", _proxies.Count);
        }

        public System.Net.WebProxy GetNextProxy()
        {
            if (_proxies.Count < 10)
                lock(_locker)
                {
                    if (_proxies.Count < 10)
                    {
                        CollectProxies();
                    }
                }
            var proxy = new WebProxy();
            _proxies.TryDequeue(out proxy);
            return proxy;
        }

        public void Success(System.Net.WebProxy proxy)
        {
            lock (_locker)
            {
                ChangeSucessRate(proxy, 1);
            }
            _proxies.Enqueue(proxy);
        }

        public void Fail(System.Net.WebProxy proxy)
        {
            lock (_locker)
            {
                ChangeSucessRate(proxy, -1);

                if (_errors[proxy.Address.Host + ":" + proxy.Address.Port] > MaximumErrorsRate)
                {
                    _errors.Remove(proxy.Address.Host + ":" + proxy.Address.Port);
                    return;
                }
                _proxies.Enqueue(proxy);
            }
        }

        private static void ChangeSucessRate(WebProxy proxy, int rate)
        {
            if (_errors.ContainsKey(proxy.Address.Host + ":" + proxy.Address.Port))
            {
                _errors[proxy.Address.Host + ":" + proxy.Address.Port] += rate;
            }
            else
            {
                _errors.Add(proxy.Address.Host + ":" + proxy.Address.Port, 1);
            }
        }
    }
}
