﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Interfaces.ParsingItems;
using Ninject;
using NUnit.Framework;
using Interfaces;

namespace Tests
{
    [TestFixture]
    class ProductPageTest
    {
        private IDataCollector _finder;

        private HtmlWeb _unicodeWeb; 

        [SetUp]
        public void Initialize()
        {
            IKernel kernel = new StandardKernel();
            kernel.Load("Parsing.Onliner.dll");
            kernel.Load("SpySProxyProvider.dll"); 
            _finder = kernel.Get<IDataCollector>();
            _unicodeWeb = new HtmlWeb
            {
                AutoDetectEncoding = false,
                OverrideEncoding = Encoding.UTF8
            };
        }

        [Test]
        [TestCase(@"Samples\Products\HightlightInTheMiddleIsMinimum.htm", 80, "http://2118.shop.onliner.by/", Currency.USD)]
        [TestCase(@"Samples\Products\HightlightInTheTopIsMinimum.htm", 1000, "http://203.shop.onliner.by/", Currency.USD)]
        [TestCase(@"Samples\Products\IgnoredIsMinimum.htm", 60, "http://10628.shop.onliner.by/", Currency.USD)]
        [TestCase(@"Samples\Products\ManyShopsInListFirstIsInStock.htm", 212, "http://11109.shop.onliner.by/", Currency.USD)]
        [TestCase(@"Samples\Products\ManyShopsInListHightlightInTheMiddle.htm", 91, "http://5718.shop.onliner.by/", Currency.USD)]
        [TestCase(@"Samples\Products\ManyShopsInListHightlightInTheTop.htm", 1250, "http://203.shop.onliner.by/", Currency.USD)]
        [TestCase(@"Samples\Products\ManyShopsInListMulipleHightlightInTheMiddle.htm", 377, "http://7509.shop.onliner.by/", Currency.USD)]
        [TestCase(@"Samples\Products\OneShopInListInStock.htm", 34, "http://5000.shop.onliner.by/", Currency.USD)]
        [TestCase(@"Samples\Products\NoShopInListIn.htm", 4, "", Currency.USD, ExpectedException = typeof(NoPricesFoundException))]
        [TestCase(@"Samples\Products\OneShopInListOutOfStock.htm", 4, "q", Currency.USD, ExpectedException = typeof(NoInStockPricesFoundException))]
        public void FindMinimalPriceInStock(string link, int price, string shop, Currency currency)
        {
            var prices = _finder.GetAllPricesItemsFromProduct(new ProductItem("", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, link), new ManufacturerItem("", "", new CategoryItem("", ""))), currency).ToList();
            if (!prices.Any())
                throw new NoPricesFoundException();
            var instock = prices.Where(s => s.IsInStock).ToList();
            if (!instock.Any())
                throw new NoInStockPricesFoundException();
            var minprice = instock.Min(s => s.Price);
            var priceEntity = instock.First(s => s.Price == minprice);
            Assert.AreEqual(priceEntity.Price, price);
            Assert.AreEqual(priceEntity.ShopUrl, shop);
        }

        [Test]
        [TestCase(@"Samples\Products\HightlightInTheMiddleIsMinimum.htm", 20, 18)]
        [TestCase(@"Samples\Products\HightlightInTheTopIsMinimum.htm", 21, 18)]
        [TestCase(@"Samples\Products\ManyShopsInListFirstIsInStock.htm", 20, 16)]
        [TestCase(@"Samples\Products\ManyShopsInListHightlightInTheMiddle.htm", 20, 18)]
        [TestCase(@"Samples\Products\ManyShopsInListHightlightInTheTop.htm", 21, 18)]
        [TestCase(@"Samples\Products\ManyShopsInListMulipleHightlightInTheMiddle.htm", 42, 33)]
        [TestCase(@"Samples\Products\NoShopInListIn.htm", 0, 0)]
        [TestCase(@"Samples\Products\OneShopInListInStock.htm", 1, 1)]
        [TestCase(@"Samples\Products\OneShopInListOutOfStock.htm", 1, 0)]
        public void CheckAllAndInStockItemsCount(string link, int items, int inStock)
        {
            var prices = _finder.GetAllPricesItemsFromProduct(new ProductItem("", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, link), new ManufacturerItem("", "", new CategoryItem("", ""))), Currency.USD).ToList();
            Assert.AreEqual(items, prices.Count);
            Assert.AreEqual(inStock, prices.Count(s=>s.IsInStock));
        }

        //Логика такая, что ежели написали в комментах к цене "Возможен безналичный расчет." то все равно собираем, и похуй на онлайнеровские чекалки вверху.
        [Test]
        [TestCase(@"Samples\Products\HightlightInTheMiddleIsMinimum.htm", 20, 18, 1, 18, 1, 16, 1)]
        [TestCase(@"Samples\Products\HightlightInTheTopIsMinimum.htm", 21, 18, 5, 12, 5, 10, 5)]
        [TestCase(@"Samples\Products\ManyShopsInListFirstIsInStock.htm", 20, 16, 8, 12, 8, 12, 8)]
        [TestCase(@"Samples\Products\ManyShopsInListHightlightInTheMiddle.htm", 20, 18, 1, 18, 1, 16, 1)]
        [TestCase(@"Samples\Products\ManyShopsInListHightlightInTheTop.htm", 21, 18, 5, 12, 5, 10, 5)]
        [TestCase(@"Samples\Products\ManyShopsInListMulipleHightlightInTheMiddle.htm", 42, 33, 7, 21, 6, 19, 5)]
        [TestCase(@"Samples\Products\NoShopInListIn.htm", 0, 0, 0, 0, 0, 0, 0)]
        [TestCase(@"Samples\Products\OneShopInListInStock.htm", 1, 1, 0, 1, 0, 1, 0)]
        [TestCase(@"Samples\Products\OneShopInListOutOfStock.htm", 1, 0, 0, 1, 0, 0, 0)]
        public void CheckInStockCreditClearingCount(string link, int all, int inStock, int credit, int clearing, int instockAndCredit, int instockAndClearing, int instockAndClearingAndCredit)
        {
            var prices = _finder.GetAllPricesItemsFromProduct(new ProductItem("", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, link), new ManufacturerItem("", "", new CategoryItem("", ""))), Currency.USD);
            Assert.AreEqual(all, prices.Count);
            Assert.AreEqual(inStock, prices.Count(s => s.IsInStock));
            Assert.AreEqual(credit, prices.Count(s => s.IsCreditPossible));
            Assert.AreEqual(clearing, prices.Count(s => s.IsClearingPossible));
            Assert.AreEqual(instockAndCredit, prices.Count(s => s.IsInStock && s.IsCreditPossible));
            Assert.AreEqual(instockAndClearing, prices.Count(s => s.IsInStock && s.IsClearingPossible));
            Assert.AreEqual(instockAndClearingAndCredit, prices.Count(s => s.IsInStock && s.IsCreditPossible && s.IsClearingPossible));
        }


        [TestCase(@"Samples\Manufacturers\OnePage.htm", 6, 6)]
        [TestCase(@"Samples\Manufacturers\OnePageNoInstockProducts.htm", 0, 6)]
        [TestCase(@"Samples\Manufacturers\OnePageNoInstockProducts2.htm", 0, 4)]
        public void CollectProducts(string link, int inStock, int outOfStock)
        {
            var products = _finder.GetAllProductsFromManufacturer(new ManufacturerItem("", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, link), new CategoryItem("","")));
            Assert.AreEqual(inStock, products.Count(s => s.IsAvailable));
            Assert.AreEqual(outOfStock, products.Count(s => !s.IsAvailable));
        }

        [TestCase(@"Samples\Manufacturers\Videocards.htm", 23)]
        [TestCase(@"Samples\Manufacturers\Monitors.htm", 22)]
        [TestCase(@"Samples\Manufacturers\Notebooks.htm", 17)]
        public void CollectManufacturers(string link, int count)
        {
            var manufacturers = _finder.GetAllManufacturersFromCategory(new CategoryItem("Some category", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, link)));
            Assert.AreEqual(count, manufacturers.Count);
        }
    }

}
