function eval_(object){if(document.getElementById)return document.getElementById(object); if(document.all)return eval('document.all.'+object); return false;}
function toggle(objid){f=1;try{o=new Enumerator(objid);}catch(e){f=0;}if(f){s=o.item().style.display=="none"?"":"none";do{o.item().style.display=s;o.moveNext();}while(!o.atEnd());}else{objid.style.display=objid.style.display=="none"?"":"none";}}
function mlit(target,action){target.style.background=action?"#F1F4F5":"#FFFFFF";}
function toggleOne(object){
	if(!(obj=eval_(object)))return false;
	if(!obj.style)return false;
	if(obj.style.display=="none")obj.style.display="";
	else obj.style.display="none";
}
function togglePlus(object){
	if(!(img=eval_(object+'_bullet')))return false;
	if(!(obj=eval_(object)))return false;
	if(!obj.style)return false;
	if(obj.style.display=="none"){
		obj.style.display="";
		img.src=iconsPath+"expand.gif";
	} else {
		obj.style.display="none";
		img.src=iconsPath+"collapse.gif";
	}
}


function go(obj) {
    var location = '/';
    if (obj.options[obj.selectedIndex].value != '' ) {
        location += obj.options[obj.selectedIndex].value + '/';
    }
    document.location = location;
}

function par(par_id){
    window.open("/paramcomment/"+par_id+"/","","width=450,height=350,resizable,scrollbars");
}

function setCookie (name, value, expires, path, domain, secure) {
        if (!path)  path="/";
        if (!expires) {
            date = new Date();
            date.setTime(date.getTime() + (3 * 60 * 60 * 1000));
            expires=date.toUTCString();
        } else if(expires == "region") {
            date = new Date();
            date.setTime(date.getTime() + (5 * 365 * 24 * 60 * 60 * 1000));
            expires=date.toUTCString();
        }
        document.cookie = name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}

$(document).ready(function(){

	$('.b-catalog-rating-1__help-wrapper span').click(function(){
		$('.b-catalog-rating-1__popup').toggle();
		return false;
	});

	$('.b-catalog-rating-1__smiles li a').click(function(){
                var sectionId = $('#b_catalog_rating').attr('sectionId');
                var answer = $(this).parent().attr('class');
                var redirect = $(this).attr('redirect_url');

                $.ajax({
                    url: "userPoll/vote/"+sectionId+"/"+answer + "?token=" + Math.floor((Math.random()*10000000)+1),
                    dataType: "json",
                    success: function(data){
                        if(data.error < 0){
                            document.location.href = data.href+encodeURIComponent(redirect+'#b_catalog_rating');
                        }
                        else if(data.error) {
                            ONotice.notify("���-�� ����� �� ���",3);
                        }
                        else {
                            $('.b-catalog-rating-1__smiles').hide();
                            $('.b-catalog-rating-1__thanks').show();
                        }
                    },
                    error: function(){
                            ONotice.notify("���-�� ����� �� ���",3);
                    }
                });

		return false;
	});

	$(document).click(function(e){
		$('.b-catalog-rating-1__popup:visible').hide();
	});

});