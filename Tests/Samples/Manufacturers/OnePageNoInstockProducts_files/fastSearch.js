var globalSearchController = {
    ch: null,
    curtab: null,

    initWithObject: function(obj, fsobj) {
        var form = document.getElementById('globalSearch');
        var input = document.getElementById('search-input');
        this.ch = obj;
        this.ch.bind('click', {curobj: this, 'fsobj': fsobj}, function(e){
            if (e.data.curobj.curtab == this.getAttribute('id')) {
                return false;
            }

            e.data.fsobj.tplType = e.data.curobj.curtab = this.getAttribute('id');

            switch(this.getAttribute('id'))
            {
                case 'search_phpbb3':
                    e.data.fsobj.searchUrl = document.location.protocol +'//'+ window.location.hostname +'/gapi/search/forum/topic.json';
                    form.action = FastSearchUrl.search_phpbb3;
                    form.method = 'GET';
                    input.setAttribute('name', 'q');
                    break;

                case 'search_baraholkanew':
                    e.data.fsobj.searchUrl = document.location.protocol +'//'+ window.location.hostname +'/gapi/search/baraholka/topic.json';
                    form.action = FastSearchUrl.search_baraholkanew;
                    form.method = 'GET';
                    input.setAttribute('name', 'q');
                    break;

                case 'search_catalogg':
                    e.data.fsobj.searchUrl = document.location.protocol +'//'+ window.location.hostname +'/gapi/search/catalog/device.json';
                    form.action = FastSearchUrl.search_catalogg;
                    form.method = 'POST';
                    input.setAttribute('name', 'search_text');
                    break;

                case 'compare-search-input':
                    e.data.fsobj.searchUrl = document.location.protocol +'//'+ window.location.hostname +'/gapi/search/catalog/devicescompare.json';
                    form.action = FastSearchUrl.search_catalogg;
                    form.method = 'POST';
                    input.setAttribute('name', 'search_text');
                    break;

		case 'search_allnews':
                    e.data.fsobj.searchUrl = null;
                    e.data.fsobj.removeSearchContainer();
                    break;

                default:
                    e.data.fsobj.searchUrl = null;
            }

            if (e.data.fsobj.searchUrl !== null && e.data.fsobj.input.val().length > 0) {
                e.data.fsobj.input.trigger('keydown');
            }
        })

        this.ch.filter('.onlsearch__tabs__active').trigger('click');
        this.curtab = this.ch.filter('.onlsearch__tabs__active').attr('id');

        $('.fs-item').live('click', function(e){
            window.location = $(this).find('a').attr('href');
        });
    }
};

function FastSearch(target, appearTarget, searchUrl, tplType) {
	this.input = $(target);
	this.searchResults = null;
	this.timeoutId = null;
	this.cursorPos = -1;
    this.tplType = tplType || null;
    this.appearTarget = appearTarget;
	this.items = [];
	this.searchUrl = searchUrl || document.location.protocol +'//'+ window.location.hostname +'/gapi/search/forum/topic.json';
	this.bindEvents();
}

function getCookie(c_name)
{
    var i,x,y,ARRcookies=document.cookie.split(";");
    for (i=0;i<ARRcookies.length;i++)
    {
        x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
        y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
        x=x.replace(/^\s+|\s+$/g,"");
        if (x==c_name)
        {
            return unescape(y);
        }
    }
}


FastSearch.prototype.bindEvents = function() {
	// docuemnt global click
	var doc = $(document);
	var globalClick = function(e) {
		var id = e.target.getAttribute('id');
		if ($.inArray(id, ['search_container', 'search-input', 'fleaMarketSearchInput']) == -1) {
			e.data.curobj.removeSearchContainer();
			doc.unbind('click', this);
		}
	};

	this.input.closest('form').bind('submit', {curobj: this}, function(e) {
            if (e.data.curobj.input.hasClass('search_example') || jQuery.trim(e.data.curobj.input.val()) == '') {
                e.preventDefault();
                return false;
            }

            if (globalSearchController.curtab == 'search_baraholkanew') {
                window.location = FastSearchUrl[globalSearchController.curtab] + '?q=' + encodeURIComponent(document.getElementById('globalSearch').search_text.value) + '&topicRegion=1';
                return false;
            } else if (globalSearchController.curtab == 'search_phpbb3') {
                window.location = FastSearchUrl[globalSearchController.curtab] + '?q=' + encodeURIComponent(document.getElementById('globalSearch').search_text.value);
                return false;
            }

            if (e.data.curobj.tplType == 'search_allnews') {
                var charsetInput = $('input[name="charset"]', this);
                window.location = FastSearchUrl.search_allnews + e.data.curobj.input.val() + (charsetInput.val() ? '&charset=' + charsetInput.val() : '');

                e.preventDefault();
                return false;
            }
        });

        this.input.bind('focus', {curobj: this}, function(e) {
            doc.unbind('click', globalClick).bind('click', e.data, globalClick);
            if (e.data.curobj.items.length > 0
                && !doc.is(':has(#search_results)')
                && globalSearchController.curtab != 'search_news'
                && globalSearchController.curtab != 'search_allnews'
            ) {
                    e.data.curobj.setTimer();
            }
        });

	this.input.bind('keydown', {curobj: this}, function(e) {
		if (e.data.curobj.searchUrl == null) {
			return;
		}

		if (e.which != 9 && e.which != 27 && e.which != 13 && e.which != 16
			&& e.which != 37 && e.which != 38 && e.which != 39 && e.which != 40)
		{
			e.data.curobj.setTimer();
		}
		else if (e.which == 13 && e.data.curobj.cursorPos != -1) {
			e.data.curobj.enterKeyPress();
			return false;
		}
		else if (e.which == 38 || e.which == 40) {
			if (!(e.data.curobj.searchResults instanceof jQuery) && e.data.curobj.items.length > 0) {
				e.data.curobj.draw();
			}
			e.data.curobj.moveCursor(e.which);
			return false;
		}
		else if (e.which == 27) {
			e.data.curobj.removeSearchContainer();
			return false;
		}
	});
};

FastSearch.prototype.setTimer = function() {
	var self = this;
	if (this.timeoutId) {
		clearTimeout(this.timeoutId);
	}
	this.timeoutId = setTimeout(function(){ self.find()}, 180);
};


FastSearch.prototype.removeSearchContainer = function() {
	if (this.searchResults instanceof jQuery) {
		this.searchResults.remove();
		this.searchResults = null;
	}
	this.cursorPos = -1;
};

FastSearch.prototype.enterKeyPress = function() {
        if (this.tplType == 'compare-search-input') {
            $('#search_container').children('div:eq('+ (this.cursorPos) +')').click();
        }
        else {
            window.location = $('#search_container').children('div:eq('+ (this.cursorPos) +')').find('a').attr('href');
        }

};

FastSearch.prototype.moveCursor = function(k) {
	var key = {
		up: 38,
		down: 40
	};

    var length = this.items.length - 1;
    if (this.tplType == 'search_baraholkanew') {
        length = this.items.length;
    }
	if (key.down == k) {
		this.cursorPos++;
		if (this.cursorPos > length) {
			this.cursorPos = -1;
		}
	}
	else if (key.up == k) {
		this.cursorPos = this.cursorPos > -1 ? this.cursorPos - 1 : length;
		if (this.cursorPos < 0) {
			this.cursorPos = -1;
		}
	}
    if ($('#search_container').children('div:eq('+ this.cursorPos +')').hasClass('b-top-search-results__regions')) {
        this.moveCursor(k);
        return false;
    }

	this.markSelected();
};

FastSearch.prototype.markSelected = function() {
	var container = $('#search_container');
	container.find('div.fs-item-selected').removeClass('fs-item-selected');

	if (this.cursorPos >= 0) {
		container.children('div:eq('+ this.cursorPos +')').addClass('fs-item-selected');
	}
};

FastSearch.prototype.template = function() {
	var rows = '';

    if (this.tplType == 'search_catalogg') {
        rows = this.catalogTemplate(this.items);
    }
    else if (this.tplType == 'compare-search-input') {
        rows = this.compareTemplate(this.items);
    }
    else {
        if (this.tplType == 'search_baraholkanew') {
            var selectedRegionInfo = getCookie('region');
            if (selectedRegionInfo !== undefined && typeof Regions !== "undefined") {
                var selectedRegion = selectedRegionInfo.split('=')[1];
                if (Regions.city[selectedRegion] !== undefined) {
                    rows = '<div class="b-top-search-results__regions">' + Regions.city[selectedRegion] + '</div>';
                } else if (Regions.region[selectedRegion] !== undefined) {
                    rows = '<div class="b-top-search-results__regions">' + Regions.region[selectedRegion] + ' область</div>';
                }
            }
            if (this.input.attr('id') !== 'fleaMarketSearchInput' || selectedRegion === undefined || selectedRegion === undefined) {
                if (this.items[0].header == true) {
                    rows = '<div class="b-top-search-results__regions">' + this.items[0].name + '</div>';
                    this.items.splice(0, 1);
                }
            }
        }
        for (var i=0; i<this.items.length; i++) {
            rows += '<div class="fs-item">';
            if (this.tplType == 'search_baraholkanew')
            {
                rows += '<div class="fs-item-fleamarket-link"><span class="fs-ba-label fs-ba-label-'+ this.items[i].category
                     +'" style="margin-right:10px;"></span>'
                     + this.items[i].link +'</div>';

                if (this.items[i].price > 0)
                {
                     rows += '<div class="fs-item-ba-price">'
                           + this.items[i].price + ' <span>'+ this.items[i].currency +'</span>'
                           + '<span class="fs-item-ba-torg">'+ this.items[i].bargain +'</span></div>';
                }
            }
            else
            {
                rows += '<div class="fs-item-forumlink"><span></span>'+ this.items[i] +'<i class="fs-clip-shadow"></i></div>';
            }
            rows += '</div>';
        }
    }

	return '<div id="search_results" style="display:block;">'
			+ '<div id="inner_search_results" style="width:'+ $(this.appearTarget).outerWidth() +'px">'
			+ '<div id="search_container">'+ rows +'</div>'
            + '<i class="s_wbg-pt"><i class="wbg-pt__i"><i class="wbg__a"></i>'
            + '<i class="wbg__a__r"></i><i class="wbg__b"></i><i class="wbg__c"></i>'
            + '<i class="wbg__d"></i><i class="wbg__d__r"></i></i></i>'
            + '</div></div>';
};

FastSearch.prototype.catalogTemplate = function(data) {
    if (!data) {
        return '';
    }

    var html = $('<div/>');
    var item = null;
    for(var i=0; i<data.length; ++i)
    {
        item = data[i];
        //контейнер для одного девайса
        var h_item_div = $('<div/>').addClass('s_item');
        if(item.is_cat == true)
        {
            h_item_div.addClass('s_catitem');
        }
        h_item_div.append( $('<div/>').addClass('s_icon').append(item.pic) );
        //инфа справа от картинки
        var s_details = $('<div/>').addClass('s_details').appendTo(h_item_div);
        //имя девайса со звездочками
        $('<div/>').addClass('s_item_name').append( item.top ).appendTo(s_details);
        $('<div/>').addClass('s_description').append( item.mid ).appendTo(s_details);
        $('<div/>').addClass('s_price').append( item.bot ).appendTo(s_details);
        h_item_div.appendTo(html);
    }

    return html.html();
};

FastSearch.prototype.compareTemplate = function(data) {
    if (!data) {
        return '';
    }

    var html = $('<div/>');
    var item = null;
    for(var i=0; i<data.length; ++i)
    {
        item = data[i];

        if(item.is_cat == false)
        {
	        //контейнер для одного девайса
	        var h_item_div = $('<div/>').addClass('s_item').addClass('s_compare').css('height','85px');
	            //h_item_div.addClass('s_catitem');
	        h_item_div.append( $('<div/>').addClass('s_icon').append(item.pic) );
	        //инфа справа от картинки
	        var s_details = $('<div/>').addClass('s_details').appendTo(h_item_div);
	        //имя девайса со звездочками
	        $('<div/>').addClass('s_item_name').append( item.top ).appendTo(s_details);
	        $('<div/>').addClass('s_description').append( item.mid ).appendTo(s_details);
	        $('<div/>').addClass('s_price').append( item.bot ).appendTo(s_details);
	        $('<div/>').addClass('s_device').append(item.dev_info).appendTo(s_details);
	        h_item_div.appendTo(html);
        }
    }

    return html.html();
};

FastSearch.prototype.draw = function() {
	$('#search_results').remove();
    if (this.items.length < 1) {
        return;
    }
    if(this.tplType=='compare-search-input'){
		this.searchResults = $(this.template()).appendTo(this.appearTarget)
		.css({
			position: 'absolute',
			top: 66,
			padding: 0,
			zIndex: 9999,
			left: this.input.position().left,
			width: 315
		});
    } else {
		this.searchResults = $(this.template()).appendTo(this.appearTarget)
		.css({
			position: 'absolute',
			top: this.input.outerHeight() + 3,
			padding: 0,
			zIndex: 9999
		});
    }
};

FastSearch.prototype.find = function() {
	if (jQuery.trim(this.input.val()).length < 1) {
		this.removeSearchContainer();
		return;
	}

	$.ajax({
		type: 'GET',
		url: this.searchUrl,
		data: {s: this.input.val()},
		dataType: 'json',
		context: this,
		success: function(r) {
			this.cursorPos = -1;
			if (jQuery.isArray(r)) {
				this.items = r;
				this.draw();
			} else {
				this.items = [];
			}
		},
		error: function() {

		}
	});
};
