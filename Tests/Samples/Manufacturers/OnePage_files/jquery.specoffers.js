$(document).ready(function () {
    $("#spec_offers").each(function () {
        $.specoffers.shaddow();
        $.specoffers.gotospec();

    });
});
$.specoffers = {
    gotospec: function (link) {
        $('#spec_offers .stars a').live("click", function (e) {
            e.preventDefault();
            e.stopPropagation();
            window.top.location.href = $(this).attr("href");
        });
        $("table.offer_shadow", "#spec_offers").live("click", function () {
            window.top.location.href = $(this).attr("link");
        });
    },

    shaddow: function () {
        $("table.offer_shadow", "#spec_offers").live("mouseover mouseout",
            function (event) {
                if (event.type == "mouseover") {
                    var _this = $(this);
                    _this.addClass("active");
                    if ($.browser.msie) {
                        _this.css({
                            top: "-10px",
                            left: "-10px"
                        });
                    }
                } else {
                    $(this).removeClass("active");
                }
            });
    }
};
