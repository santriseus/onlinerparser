$(function () {
    var $weatherInformer = $('.top-informer-weather');

    $weatherInformer.live('mouseenter', '.phenomena-icon', function () {
        var position = $(this).offset();
        $('body').append('<div class="b-weather-tooltip"></div>');
        $('.b-weather-tooltip')
            .css({'top': position.top - 20, 'left': position.left - 20})
            .html($('.phenomena-icon.extra-small').attr('data-phenomena'))
            .fadeIn(90);
    });

    $weatherInformer.live('mouseleave', '.phenomena-icon', function () {
        $('.b-weather-tooltip').fadeOut(80, function () {
            $(this).remove();
        });
    });
});
