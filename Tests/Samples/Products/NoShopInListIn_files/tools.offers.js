var Onliner = Onliner || {};

Onliner.Offers = {
    config: {
        cookie_region: 'catalog_region_select',
        privileged_region: 'minsk',
        truncate_limit: 5,
        currency_byr: 'руб.',
        currency_usd: 'у.е.'
    },

    isMobile: false,

    keyword: '',

    modifiers: {
        collapsed: 'm-collapsed',
        expanded: 'm-expanded',
        filtered: 'm-filtered',
        highlighted: 'm-highlighted',
        empty: 'm-empty'
    },

    nodes: {},

    selectors: {
        position_item: '.js-position-item',
        position_wrapper: '.js-position-wrapper',
        region_expand: '.js-region-expand',
        shop: '.js-shop',
        sort: '.js-sort',
        filter: '.js-filter',
        filter_not_found: '.js-filter-not-found',
        currency: '.js-currency',
        currency_primary: '.js-currency-primary',
        currency_secondary: '.js-currency-secondary',
        typesearch: '.js-typesearch',
        typesearch_reset: '.js-typesearch-reset',
        typesearch_not_found: '.js-search-not-found',
        comment: '.js-comment'
    },

    state: {
        region: '',
        sort_by: '',
        sort_order: '',
        currency: 'usd',
        instock: false,
        credit: false,
        cashless: false
    },

    positions: {},

    regions: [],

    towns: {}
};

Onliner.Offers.setState = function (key, value) {
    if (key === 'currency' && (_.indexOf(['usd', 'byr'], value) < 0)) {
        value = 'usd';
    }

    this.state[key] = value;
    this.setHash();
};

/**
 * Regions
 */

Onliner.Offers.truncateRegion = function (region) {
    var $region = this.getRegionNode(region),
        $positions = $region.find(this.selectors.position_item + '.' + this.modifiers.filtered),
        count = {
            positions_global: $region.find(this.selectors.position_item).length,
            positions_total: $positions.length,
            positions_showed: 0
        },
        limit = this.config.truncate_limit,
        shops = [];

    if (!count.positions_total || $region.hasClass(this.modifiers.empty)) {
        return false;
    }

    if (count.positions_total < limit) {
        limit = count.positions_total;
    }

    $region.find(this.selectors.position_item).hide();

    $.each($positions, function (iter, item) {
        var $item = $(item),
            shop = parseInt($item.data('shop'));

        // one shop one position
        if (_.indexOf(shops, shop) < 0) {
            count.positions_showed++;
            shops.push(shop);
            $item.show();
        }

        if (shops.length === limit) {
            return false;
        }
    });

    this.setCounter(region, shops.length);

    if (count.positions_global > count.positions_showed) {
        $region.addClass(this.modifiers.collapsed);
    } else {
        $region.removeClass(this.modifiers.collapsed);
    }

    if (count.positions_total > count.positions_showed) {
        $region.find(this.selectors.region_expand).show();
    } else {
        $region.find(this.selectors.region_expand).hide();
    }

    $region.show();
};

Onliner.Offers.getRegion = function () {
    var region = '';

    if (this.state.region) {
        region = this.state.region;
    } else {
        region = $.cookie(this.config.cookie_region);
        this.state.region = region;
    }

    return region;
};

Onliner.Offers.setRegion = function (region) {
    this.setRegionCookie(region);
    this.state.region = region;

    return region;
};

Onliner.Offers.syncRegionTitle = function () {
    var region = this.state.region;

    if (this.towns[region]) {
        var title = document.title,
            header = this.nodes.$header.text(),
            replacer = this.towns[region].prepCase,
            length;

        // document's title
        title = title.split(' ');

        $.each(title, function (iter, word) {
            if (word === 'по') {
                if (title[iter - 2] && title[iter - 2] === 'в') {
                    title[iter - 1] = replacer;
                    return false;
                } else {
                    title.splice(iter, 0, 'в', replacer);
                    return false;
                }
            }
        });

        document.title = title.join(' ');

        // page's header
        header = header.split(' ');
        length = header.length;

        if (header[length - 2] === 'в') {
            header[length - 1] = replacer;
        } else {
            header.push('в');
            header.push(replacer);
        }

        this.nodes.$header.text(header.join(' '));
    }
};

Onliner.Offers.parseRegion = function () {
    var region = this.getRegion(),
        $privileged_region = this.getRegionNode(this.config.privileged_region),
        self = this,
        is_offers_in_region = this.positions[region] ? true : false;

    this.regions = [];

    if (is_offers_in_region) {
        this.regions.push(region);

        if (region === this.config.privileged_region) {
            this.getRegionNode(region).removeClass(this.modifiers.collapsed);
        }

        if (this.positions[this.config.privileged_region] && region !== this.config.privileged_region) {
            this.regions.push(this.config.privileged_region);
        }

        $privileged_region.appendTo(this.nodes.$wrapper);

        this.syncRegionTitle();
    } else {
        $.each(this.positions, function (key) {
            self.regions.push(key);
        });

        $privileged_region.prependTo(this.nodes.$wrapper);
    }

    this.setTab(region);
    this.resetState();
    this.execForRegions(this.resetCounter);
    this.execForRegions(this.showRegion);
    this.initCurrency();
    this.initSort();
    this.initFilter();
    this.mergeOffers();

    if (is_offers_in_region) {
        this.expandRegion(region);

        if (this.state.region !== this.config.privileged_region && !this.keyword) {
            this.truncateRegion(this.config.privileged_region);
        }
    } else {
        this.execForRegions(this.truncateRegion);
    }
};

Onliner.Offers.showRegion = function (region) {
    var $region = this.getRegionNode(region);

    if (this.state.region === region) {
        $region.find(this.selectors.region_expand).hide();
    }

    $region.show();
};

Onliner.Offers.expandRegion = function (region) {
    var $region = this.getRegionNode(region);

    if ($region.hasClass(this.modifiers.collapsed)) {
        $region.removeClass(this.modifiers.collapsed);
        $region.addClass(this.modifiers.expanded);
        $region.find(this.selectors.region_expand).hide();
        $region.find(this.selectors.position_item).show();

        this.filtrateOffers();
    }
};

Onliner.Offers.getRegionNode = function (region) {
    return $('#region-' + region);
};

Onliner.Offers.resetState = function () {
    this.nodes.$regions.hide();
    this.nodes.$items.show();
};

Onliner.Offers.syncRegion = function () {
    var region = '';

    if (this.state.region) {
        region = this.state.region;

        this.setRegionCookie(region);
    } else {
        if ($.cookie(this.config.cookie_region)) {
            region = $.cookie(this.config.cookie_region);
        } else {
            region = this.config.privileged_region;

            this.setRegionCookie(this.config.privileged_region);
        }

        this.setState('region', region);
    }
};

/**
 * Offers
 */

Onliner.Offers.getOfferNode = function (offer_region, offer_id) {
    return $('#position-' + offer_region + '-' + offer_id);
};

/**
 * Counters
 */

Onliner.Offers.getCounter = function (region) {
    var $counter = this.getCounterNode(region);

    return {
        current: $counter.data('current'),
        initial: $counter.data('initial'),
        total: $counter.data('total')
    }
};

Onliner.Offers.getCounterNode = function (region) {
    return $('#counter-' + region);
};

Onliner.Offers.setCounter = function (region, count) {
    var $counter = this.getCounterNode(region),
        word = '',
        string = '';

    if (count !== 0) {
        word = this.getPluralForm(count);
        string = 'от ' + count + ' ' + word;
    }

    $counter.data('current', count);
    $counter.html(string);
};

Onliner.Offers.setCounterTotal = function (region, count) {
    var $counter = this.getCounterNode(region);

    $counter.data('total', count);
};

Onliner.Offers.resetCounter = function (region) {
    var $counter = this.getCounterNode(region),
        initial = $counter.data('initial');

    this.setCounter(region, initial);
    this.setCounterTotal(region, initial);
};

/**
 * Binds
 */

Onliner.Offers.bindEvents = function () {
    var self = this;

    // Stars
    var $offerStar = $('.b-offers .js-star');
    $offerStar.click(function () {
        $offerStar.css("opacity", 0.5);
        if ($(this).hasClass('off')) {
            $.getJSON(
                '/bookmark_device.php',
                {'dev_id': $offerStar.data('id')},
                function (data) {
                    $offerStar.css("opacity", 1);
                    if (!data || !data.success) {
                        return;
                    }
                    $offerStar.addClass('on').removeClass('off');
                    $('.star', $offerStar).text('★');
                    $('u', $offerStar).text('Удалить из закладок');
                }
            );
        } else {
            $.getJSON(
                '/bookmark_device.php',
                {'bm_id': $offerStar.data('id')},
                function (data) {
                    $offerStar.css("opacity", 1);
                    if (!data || !data.success) {
                        return;
                    }
                    $offerStar.addClass('off').removeClass('on');
                    $('.star', $offerStar).text('☆');
                    $('u', $offerStar).text('Добавить в закладки');
                }
            );
        }
        return false;
    });

    $('.js-region-select').on('click', function (event) {
        var $tab = $(this);

        if ($tab.hasClass('selected')) {
            return false;
        }

        $('.js-region-select').removeClass('selected');
        $tab.addClass('selected');

        self.setRegion($tab.data('region'));
        self.setHash();
        self.parseRegion();

        event.preventDefault();
    });

    $(self.selectors.region_expand).on('click', function (event) {
        var region = $(this).data('region'),
            $region = self.getRegionNode(region);

        self.expandRegion(region);

        $('body, html').animate({
            'scrollTop': $region.offset().top
        }, 'slow');

        $region.find(self.selectors.region_expand).hide();

        self.mergeOffers();

        event.preventDefault();
    });

    $(self.selectors.currency).on('click', function (event) {
        var $tab = $(this);

        if ($tab.hasClass('selected')) {
            return false;
        }

        $(self.selectors.currency).removeClass('selected');
        $tab.addClass('selected');

        self.setCurrency($tab.data('currency'));

        // sort again due to currency exchange rates
        self.routeSort();
        self.mergeOffers();

        event.preventDefault();
    });

    $(self.selectors.sort).on('click', function (event) {
        var $sort = $(this),
            css_sorted = 'sorted',
            sort_by = $sort.data('sort'),
            sort_order,
            sort_direction;

        // Already sorted ?
        if ($sort.hasClass(css_sorted)) {

            $sort.toggleClass('sorted-up sorted-down');

            if ($sort.hasClass('sorted-up')) {
                sort_order = 'asc';
            } else {
                sort_order = 'desc';
            }
        } else {
            $(self.selectors.sort).removeClass(css_sorted + ' sorted-up sorted-down');

            if (sort_by === 'price') {
                sort_direction = 'sorted-up';
                sort_order = 'asc';
            } else {
                sort_direction = 'sorted-down';
                sort_order = 'desc';
            }

            self.setState('sort_by', sort_by);

            $sort.addClass(css_sorted);
            $sort.addClass(sort_direction);
        }

        self.setState('sort_order', sort_order);

        self.routeSort();
        self.mergeOffers();

        event.preventDefault();
    });

    $(self.selectors.filter).on('change', function () {
        var $checkbox = $(this);

        self.setState($checkbox.attr('name'), $checkbox.prop('checked'));
        self.filtrateOffers();
        self.mergeOffers();
    });

    $(self.selectors.typesearch).on('keyup', _.debounce(function () {
        var search = $(this).val();

        search = $.trim(search);
        search = search.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");

        if (search.length) {
            self.keyword = search;
            $(self.selectors.typesearch_reset).show();
            self.doTypesearch();
        } else {
            $(self.selectors.typesearch_reset).hide();
            self.resetTypesearch();
        }
    }, 200));

    $(self.selectors.typesearch_reset).on('click', function () {
        self.resetTypesearch();
    });
};

Onliner.Offers.bindNodes = function () {
    this.nodes = {
        $header: $('.b-offers-title'),
        $items: $(this.selectors.position_item),
        $regions: $('.js-region'),
        $wrapper: $('#js-regions-wrapper')
    }
};

/**
 * Hash
 */

Onliner.Offers.getHash = function () {
    var hash = document.location.hash,
        self = this,
        tmp;

    hash = hash.slice(1, hash.length);

    tmp = hash.split('&');

    $.each(tmp, function (iter, item) {
        var pair = item.split('=');
        self.setState(pair[0], pair[1]);
    });
};

Onliner.Offers.setHash = function () {
    var hash = '';

    $.each(this.state, function (key, value) {
        if (value) {
            hash += key + '=' + value + '&';
        }
    });

    document.location.replace(document.location.pathname + '#' + hash.slice(0, hash.length - 1));

    return hash;
};

Onliner.Offers.setTab = function (region) {
    $('#tab-' + region).trigger('click');
};

/**
 * Sort
 */

Onliner.Offers.initSort = function () {
    var sort_by = this.state.sort_by,
        sort_order = this.state.sort_order;

    if (sort_by && (sort_by === 'price' || sort_by === 'rating')) {
        if (!sort_order || !(sort_order === 'asc' || sort_order === 'desc')) {
            if (sort_by === 'price') {
                sort_order = 'asc';
            } else {
                sort_order = 'desc';
            }

            this.setState('sort_order', sort_order);
        }

        $(this.selectors.sort).filter('[data-sort=' + sort_by + ']').addClass('sorted').addClass(sort_order === 'asc' ? 'sorted-up' : 'sorted-down');

        this.routeSort();

    } else {
        this.setState('sort_by', '');
        this.setState('sort_order', '');
    }
};

Onliner.Offers.routeSort = function () {
    var sort_by = this.state.sort_by,
        sort_order = this.state.sort_order;

    if (sort_by === 'price') {
        this.sortByPrice(sort_order);
    }

    if (sort_by === 'rating') {
        this.sortByRating(sort_order);
    }
};

Onliner.Offers.sortByPrice = function (order) {
    var self = this,
        price = (this.state.currency === 'usd') ? 'priceUsd' : 'priceByr',
        is_asc = (order === 'asc') ? true : false,

        rule = function (item) {
            var sum = 0,
                factor = 1,
                global = is_asc ? 1 : -1;

            sum += item[price].amount;

            if (!_.isBoolean(item.shopPosition)) {
                // 0 in item.order will not give effect -> do plus 1
                sum += -1 * global * (1 - 0.1 * (item.shopPosition + 1));
            }

            switch (item.status) {
                case 'preorder':
                    factor = is_asc ? 1000 : 0.1;
                    break;
                case 'order':
                    factor = is_asc ? 10000 : 0.01;
                    break;
                default:
                    break;
            }

            return global * sum * factor;
        },

        doSort = function (region) {
            self.positions[region] = _.shuffle(self.positions[region]);
            self.positions[region] = _.sortBy(self.positions[region], rule);

            self.redrawAfterSort(region);
        };

    this.execForRegions(doSort);
};

Onliner.Offers.sortByRating = function (order) {
    var self = this,
        generateId = function () {
            return Math.floor(Math.random() * 1000);
        },
        doSort = function (region) {
            var rule = (order === 'asc') ? 'shopRating' : function (item) {
                return (-1 * item.shopRating);
            };

            // group by shop id
            self.positions[region] = _.sortBy(self.positions[region], 'shopId');

            // ... create groups
            (function generateRandomGroupId() {
                var group_id = generateId(),
                    shop = 0;

                _.each(self.positions[region], function (item) {
                    if (item.shopId === shop) {
                        item.groupId = group_id;
                    } else {
                        group_id = generateId();
                        item.groupId = group_id;
                    }
                    shop = item.shopId;
                });
            })();

            // ... shuffle positions
            self.positions[region] = _.shuffle(self.positions[region]);

            // ... group by group id
            self.positions[region] = _.sortBy(self.positions[region], 'groupId');

            // ... and then sort by rule
            self.positions[region] = _.sortBy(self.positions[region], rule);

            self.redrawAfterSort(region);
        };

    this.execForRegions(doSort);
};

Onliner.Offers.redrawAfterSort = function (region) {
    var self = this,
        $region = this.getRegionNode(region),
        $wrapper = $region.find(this.selectors.position_wrapper);

    // do not redraw empty block (active filters)
    if ($region.hasClass(this.modifiers.empty)) {
        return false;
    }

    $.each(this.positions[region], function (iter, item) {
        var $offer = self.getOfferNode(region, item.id);

        $offer.appendTo($wrapper);

        if ($offer.hasClass(self.modifiers.filtered)) {
            $offer.show();
        }
    });

    if ($region.hasClass(this.modifiers.collapsed)) {
        this.truncateRegion(region);
    }

    this.doTypesearch();
};

/**
 * Currency
 */

Onliner.Offers.initCurrency = function () {
    if (this.state.currency !== 'usd') {
        $(this.selectors.currency).removeClass('selected');
        $(this.selectors.currency).filter('[data-currency=' + this.state.currency + ']').addClass('selected');

        this.setCurrency(this.state.currency);
    }
};

Onliner.Offers.setCurrency = function (currency) {
    var self = this,
        update = function (region) {
            $.each(self.positions[region], function (iter, item) {
                var $offer = self.getOfferNode(region, item.id),
                    currency_primary = '',
                    currency_secondary = '';

                if (self.state.currency === 'usd') {
                    currency_primary = item.priceUsd.formatted + ' <small>' + self.config.currency_usd + '</small>';
                    currency_secondary = item.priceByr.formatted + ' ' + self.config.currency_byr;
                } else {
                    currency_primary = item.priceByr.formatted + ' <small>' + self.config.currency_byr + '</small>';
                    currency_secondary = item.priceUsd.formatted + ' ' + self.config.currency_usd;
                }

                $offer.find(self.selectors.currency_primary).html(currency_primary);
                $offer.find(self.selectors.currency_secondary).html(currency_secondary);
            });
        };

    this.setState('currency', currency);
    this.execForRegions(update);
};

/**
 * Filter
 */

Onliner.Offers.initFilter = function () {
    var self = this,
        $filters = $(this.selectors.filter),
        filters = ['instock', 'credit', 'cashless'];

    $.each(filters, function (iter, item) {
        if (self.state[item]) {
            $filters.filter('[name=' + item + ']').attr('checked', 'checked');
        }
    });

    this.filtrateOffers();
};

Onliner.Offers.filtrateOffers = function () {
    var self = this,
        filtrate = function (region) {
            var $region = self.getRegionNode(region),
                state = self.state,
                sellers = [],
                filters = [],
                count;

            $region.removeClass(self.modifiers.empty);
            self.hideFilterNotFound(region);

            if (!state.instock && !state.credit && !state.cashless) {
                $region.find(self.selectors.position_item).addClass(self.modifiers.filtered);
                $region.find(self.selectors.position_item).show();
                self.resetCounter(region);
            } else {
                $region.find(self.selectors.position_item).removeClass(self.modifiers.filtered);
                $region.find(self.selectors.position_item).hide();

                (function fillFilters() {
                    if (state.instock) {
                        filters.push('в наличии');
                    }
                    if (state.credit) {
                        filters.push('в кредит');
                    }
                    if (state.cashless) {
                        filters.push('по безналичному расчету');
                    }
                })();

                $.each(self.positions[region], function (iter, item) {
                    var $offer = self.getOfferNode(region, item.id),
                        match_stock = true,
                        match_credit = true,
                        match_cashless = true;

                    if (state.instock) {
                        if (item.status != 'stock' && item.status != 'special') {
                            match_stock = false;
                        }
                    }

                    if (state.credit) {
                        if (!item.isCredit) {
                            match_credit = false;
                        }
                    }

                    if (state.cashless) {
                        if (!item.isCashless) {
                            match_cashless = false;
                        }
                    }

                    if (match_stock && match_credit && match_cashless) {
                        $offer.addClass(self.modifiers.filtered);
                        $offer.show();

                        if ($.inArray(item.shopId, sellers) < 0) {
                            sellers.push(item.shopId);
                        }
                    }
                });

                count = sellers.length;

                self.setCounter(region, count);
                self.setCounterTotal(region, count);

                if (count === 0) {
                    $region.addClass(self.modifiers.empty);
                    self.showFilterNotFound(region, filters);
                }
            }

            if ($region.hasClass(self.modifiers.collapsed)) {
                self.truncateRegion(region);
                self.mergeOffers();
            }

            this.doTypesearch();
        };

    this.execForRegions(filtrate);
};

Onliner.Offers.hideFilterNotFound = function (region) {
    var $message = this.getRegionNode(region).find(this.selectors.filter_not_found).filter(':visible');

    $message.hide();
    $message.empty();
};

Onliner.Offers.showFilterNotFound = function (region, filters) {
    var $message = this.getRegionNode(region).find(this.selectors.filter_not_found),
        conditions = '';

    if (_.isArray(filters)) {
        conditions = filters.join(', ');
    }

    $message.html('<p>К сожалению, не найдено ни одного предложения ' + conditions + '</p>');
    $message.show();
};

/**
 * Typesearch
 */

Onliner.Offers.doTypesearch = function () {
    var regexp = new RegExp(this.keyword, 'gi'),
        self = this,
        search = function (region) {
            var $region = this.getRegionNode(region),
                $offers = $region.find(this.selectors.position_item + '.' + this.modifiers.filtered),
                counter = 0,
                shops = [];

            if ($region.hasClass(this.modifiers.collapsed)) {
                this.expandRegion(region);
            }

            $.each($offers, function (iter, item) {
                var $offer = $(item),
                    $comment = $offer.find(self.selectors.comment),
                    comment = $comment.text(),
                    shop = $offer.data('shop');

                // reset previous highlights
                $comment.html(comment);

                // comment has keyword ?
                if (comment.match(regexp)) {
                    comment = comment.replace(regexp, '<em class="marked">$&</em>');
                    $comment.html(comment);

                    if (!(_.indexOf(shops, shop) + 1)) {
                        shops.push(shop);
                    }
                    counter++;

                    $offer.addClass(self.modifiers.highlighted);
                    $offer.show();
                } else {
                    $offer.removeClass(self.modifiers.highlighted);
                    $offer.hide();
                }
            });

            if ($offers.length && counter < 1) {
                $region.find(this.selectors.typesearch_not_found).show();
            } else {
                $region.find(this.selectors.typesearch_not_found).hide();
            }

            this.setCounter(region, shops.length);
        };

    if (!this.keyword) {
        return false;
    }

    this.execForRegions(search);
    this.execForRegions(this.mergeOffers);
};

Onliner.Offers.resetTypesearch = function () {
    var $offers = $(this.selectors.position_item + '.' + this.modifiers.filtered),
        self = this;

    this.keyword = '';

    $(this.selectors.typesearch).val('');
    $(this.selectors.typesearch_reset).hide();
    $(this.selectors.typesearch_not_found).hide();

    $.each($offers, function (iter, item) {
        var $offer = $(item),
            $comment = $offer.find(self.selectors.comment),
            comment = $comment.text();

        // reset highlights
        $comment.html(comment);

        $offer.removeClass(self.modifiers.highlighted);
        $offer.show();
    });

    this.execForRegions(this.resetCounter);
};

/**
 * Helpers
 */

Onliner.Offers.execForRegions = function (callback) {
    var callback = _.isFunction(callback) ? callback : function () {},
        self = this;

    $.each(this.regions, function (iter, item) {
        callback.call(self, item);
    });
};

Onliner.Offers.getPluralForm = function (num) {
    // 'от N ...'
    var forms = ['продавца', 'продавцов'],
        form = 1;

    // 11 is exclusion
    if (num % 10 === 1 && (Math.floor(num / 10) !== 1)) {
        form = 0;
    }

    return forms[form];
};

Onliner.Offers.mergeOffers = function () {
    var merge = function (region) {
        var $offers = this.getRegionNode(region).find(this.selectors.position_item + ':visible'),
            css_divider = 'm-divider',
            self = this,
            shop_previous = parseInt($offers.eq(0).data('shop')),
            first_iteration = true;

        $.each($offers, function (iter, item) {
            var $offer = $(item),
                $shop = $offer.find(self.selectors.shop),
                shop_current = parseInt($offer.data('shop'));

            $offer.removeClass(css_divider);
            $shop.hide();

            if (shop_current !== shop_previous) {
                $offer.addClass(css_divider);
                $shop.show();
            } else if (first_iteration) {
                $shop.show();
                first_iteration = false;
            }

            shop_previous = shop_current;
        });
    };

    this.execForRegions(merge);
};

Onliner.Offers.setRegionCookie = function (value) {
    var domain = document.location.host;

    domain = domain.split('.');
    domain.shift();
    domain = domain.join('.');

    $.cookie(this.config.cookie_region, value, {
        domain: domain,
        expires: 365,
        path: '/'
    });
};

Onliner.Offers.detectRetina = function () {
    var image = document.getElementById('device-header-image');

    if (image && window.devicePixelRatio > 1) {
        image.src = image.src.replace(/\/header\//, '/header@2/');
        image.className = 'retina';
    }
};

/**
 * Init
 */

Onliner.Offers.init = function () {
    this.detectRetina();
    this.bindNodes();
    this.bindEvents();
    this.addBookmark();
    this.getHash();
    this.syncRegion();
    this.parseRegion();
};

Onliner.Offers.addBookmark = function () {
    if (document.location.hash === "#add-bookmark") {
        var $offerStar = $('.b-offers .js-star').eq(0);
        if ($offerStar.hasClass('off')) {
            $offerStar.trigger('click');
        }
    }
};

$(function initOffers() {
    Onliner.Offers.init();
});

$(function () {
	// Titles
	if($('.b-offers .js-title').length && !Onliner.Offers.isMobile) {
		var $offersBoxTitled = $('.b-offers .js-title');

		$offersBoxTitled.each(function () {
			$(this).attr('data-title', $(this).attr('title'));
			$(this).removeAttr('title');
		});

		$offersBoxTitled.on('mouseover', function (e) {
			$('#container').children().last().after('<div class="b-offers-popover-title">' + $(this).attr('data-title') + '</div>');

            var $popover = $('.b-offers-popover-title'),
                popoverOuterWidth = $popover.outerWidth();

            $popover.css({
                'top': $(this).offset().top + $(this).outerHeight(),
                'left': e.clientX,
                'marginLeft': -1 * (popoverOuterWidth / 2)
            }).fadeIn(200);
		});

		$offersBoxTitled.on('mouseleave', function (e) {
			$('.b-offers-popover-title').remove();
		});
	}


	// Selects
	$('.b-offers-desc__select').on('click', function (e) {
		e.stopPropagation();
	});

	$('#container').bind('click', function() {
		$('.b-offers-desc__select-wrapper').fadeOut(100);
	});

	$('.b-offers-desc__info-price .pseudolink').click(function(e) {
		$('.b-offers-desc__select-wrapper').fadeOut(100);
		$(this).siblings('.b-offers-desc__select-wrapper').toggle();
		e.stopPropagation();
		return false;
	});

	$(document).keydown(function(e) {
		if (e.keyCode == 27) { // Esc
			$('.b-offers-desc__select-wrapper').fadeOut(100);
		}
	});

	// Validation
	$("#offers-summ").on('change keypress keydown paste blur load', function (e) {
		if (($(this).val() < 1 || $(this).val().search(/^\d+(\.|,)?\d*$/) == -1) && $(this).val() !== "") {
			$(this).parents('.input-group').addClass('error');
		} else {
			$(this).parents('.input-group').removeClass('error');
		}
	});

	$('#offers-email, .b-offers-popup .js-valid-email').on('change keypress keydown paste blur load', function (e) {
		var emailRegExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

		if ($(this).val().search(emailRegExp) == -1) {
			$(this).parents('.input-group').addClass('error');
		} else {
			$(this).parents('.input-group').removeClass('error');
		}
	});


	// Popups
    $('.b-offers-popup').each(function () {
        $('.inp:last', $(this)).on('change keypress keydown paste blur load', function () {
            if (! $(this).parents('.b-offers-popup').find('.error').length) {
                $(this).parents('.b-offers-popup').find('.js-submit').removeAttr('disabled').removeClass('disabled');
            } else {
                $(this).parents('.b-offers-popup').find('.js-submit').attr('disabled','disabled').addClass('disabled');
            }
        });
    });

	$('.b-offers .js-remind-price, .b-offers .js-change-price, .b-offers .js-popup').on('click', function () {
		var popupGutterTop = ($(window).height() - 356) / 2;
		if (popupGutterTop < 30) {
			var popupGutterTop = 30;
		}
		// alert('#' + $(this).attr('data-popup') + '')
		$('#' + $(this).attr('data-popup') + '')
		.css({
			'top': popupGutterTop + 'px'
		})
		.fadeIn(200);
		$('.b-overlayer').fadeIn(100);
		return false;
	});

	$('.b-overlayer, .b-offers-popup .btn-close').on('click', function(e) {
		offersPopUpHide ();
		return false;
	});

	$(document).on('keydown', function(e) {
		if (e.keyCode == 27) {
			offersPopUpHide ();
		}
	});

	function offersPopUpHide () {
		$('.b-offers-popup, .b-overlayer').fadeOut(100);
	}

	// Sort trigger
	var $sortCont = $('.js-sorter', '.b-offers'),
		$sortLink = $('a', $sortCont);

	$sortLink.on('click', function () {
		$(this).parent().siblings().find('a').attr('class','');
		if (!$(this).hasClass('sorted')){
			$(this).addClass('sorted-' + $(this).attr('data-initial-sort') + ' sorted');
		} else {
			$(this).toggleClass('sorted-up sorted-down');
		}
		return false;
	});

	// Chart range

	var $chartMenu = $('.b-offers-chart__menu'),
		$chartMenuLink = $('a', $chartMenu);

	$chartMenuLink.click(function () {
		$(this).parent().siblings().removeClass('selected');
		$(this).parent().addClass('selected');
		switchDataRange($(this).attr('data-range'));
		return false;
	});

    var $topicCountPosts = $('.b-offers-subnav .topic_count_posts');
    if ($topicCountPosts.length && $topicCountPosts.data('topicid')) {
        $.ajax({
            url: "/gapi/forum/topiccount/get/" + $topicCountPosts.data('topicid'),
            dataType: "text",
            success: function(data){
                if(parseInt(data) > 0){
                    $topicCountPosts.html(data)
                }
            }
        });
    }
});

// TODO: Remove this libs to 'gc'

/**
 * jQuery Cookie
 */
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a(jQuery)}(function(a){function c(a){return h.raw?a:encodeURIComponent(a)}function d(a){return h.raw?a:decodeURIComponent(a)}function e(a){return c(h.json?JSON.stringify(a):String(a))}function f(a){0===a.indexOf('"')&&(a=a.slice(1,-1).replace(/\\"/g,'"').replace(/\\\\/g,"\\"));try{return a=decodeURIComponent(a.replace(b," ")),h.json?JSON.parse(a):a}catch(c){}}function g(b,c){var d=h.raw?b:f(b);return a.isFunction(c)?c(d):d}var b=/\+/g,h=a.cookie=function(b,f,i){if(void 0!==f&&!a.isFunction(f)){if(i=a.extend({},h.defaults,i),"number"==typeof i.expires){var j=i.expires,k=i.expires=new Date;k.setTime(+k+864e5*j)}return document.cookie=[c(b),"=",e(f),i.expires?"; expires="+i.expires.toUTCString():"",i.path?"; path="+i.path:"",i.domain?"; domain="+i.domain:"",i.secure?"; secure":""].join("")}for(var l=b?void 0:{},m=document.cookie?document.cookie.split("; "):[],n=0,o=m.length;o>n;n++){var p=m[n].split("="),q=d(p.shift()),r=p.join("=");if(b&&b===q){l=g(r,f);break}b||void 0===(r=g(r))||(l[q]=r)}return l};h.defaults={},a.removeCookie=function(b,c){return void 0===a.cookie(b)?!1:(a.cookie(b,"",a.extend({},c,{expires:-1})),!a.cookie(b))}});

/**
 * Underscore.js 1.5.2
 */
(function(){var n=this,t=n._,r={},e=Array.prototype,u=Object.prototype,i=Function.prototype,a=e.push,o=e.slice,c=e.concat,l=u.toString,f=u.hasOwnProperty,s=e.forEach,p=e.map,h=e.reduce,v=e.reduceRight,g=e.filter,d=e.every,m=e.some,y=e.indexOf,b=e.lastIndexOf,x=Array.isArray,w=Object.keys,_=i.bind,j=function(n){return n instanceof j?n:this instanceof j?(this._wrapped=n,void 0):new j(n)};"undefined"!=typeof exports?("undefined"!=typeof module&&module.exports&&(exports=module.exports=j),exports._=j):n._=j,j.VERSION="1.5.2";var A=j.each=j.forEach=function(n,t,e){if(null!=n)if(s&&n.forEach===s)n.forEach(t,e);else if(n.length===+n.length){for(var u=0,i=n.length;i>u;u++)if(t.call(e,n[u],u,n)===r)return}else for(var a=j.keys(n),u=0,i=a.length;i>u;u++)if(t.call(e,n[a[u]],a[u],n)===r)return};j.map=j.collect=function(n,t,r){var e=[];return null==n?e:p&&n.map===p?n.map(t,r):(A(n,function(n,u,i){e.push(t.call(r,n,u,i))}),e)};var E="Reduce of empty array with no initial value";j.reduce=j.foldl=j.inject=function(n,t,r,e){var u=arguments.length>2;if(null==n&&(n=[]),h&&n.reduce===h)return e&&(t=j.bind(t,e)),u?n.reduce(t,r):n.reduce(t);if(A(n,function(n,i,a){u?r=t.call(e,r,n,i,a):(r=n,u=!0)}),!u)throw new TypeError(E);return r},j.reduceRight=j.foldr=function(n,t,r,e){var u=arguments.length>2;if(null==n&&(n=[]),v&&n.reduceRight===v)return e&&(t=j.bind(t,e)),u?n.reduceRight(t,r):n.reduceRight(t);var i=n.length;if(i!==+i){var a=j.keys(n);i=a.length}if(A(n,function(o,c,l){c=a?a[--i]:--i,u?r=t.call(e,r,n[c],c,l):(r=n[c],u=!0)}),!u)throw new TypeError(E);return r},j.find=j.detect=function(n,t,r){var e;return O(n,function(n,u,i){return t.call(r,n,u,i)?(e=n,!0):void 0}),e},j.filter=j.select=function(n,t,r){var e=[];return null==n?e:g&&n.filter===g?n.filter(t,r):(A(n,function(n,u,i){t.call(r,n,u,i)&&e.push(n)}),e)},j.reject=function(n,t,r){return j.filter(n,function(n,e,u){return!t.call(r,n,e,u)},r)},j.every=j.all=function(n,t,e){t||(t=j.identity);var u=!0;return null==n?u:d&&n.every===d?n.every(t,e):(A(n,function(n,i,a){return(u=u&&t.call(e,n,i,a))?void 0:r}),!!u)};var O=j.some=j.any=function(n,t,e){t||(t=j.identity);var u=!1;return null==n?u:m&&n.some===m?n.some(t,e):(A(n,function(n,i,a){return u||(u=t.call(e,n,i,a))?r:void 0}),!!u)};j.contains=j.include=function(n,t){return null==n?!1:y&&n.indexOf===y?n.indexOf(t)!=-1:O(n,function(n){return n===t})},j.invoke=function(n,t){var r=o.call(arguments,2),e=j.isFunction(t);return j.map(n,function(n){return(e?t:n[t]).apply(n,r)})},j.pluck=function(n,t){return j.map(n,function(n){return n[t]})},j.where=function(n,t,r){return j.isEmpty(t)?r?void 0:[]:j[r?"find":"filter"](n,function(n){for(var r in t)if(t[r]!==n[r])return!1;return!0})},j.findWhere=function(n,t){return j.where(n,t,!0)},j.max=function(n,t,r){if(!t&&j.isArray(n)&&n[0]===+n[0]&&n.length<65535)return Math.max.apply(Math,n);if(!t&&j.isEmpty(n))return-1/0;var e={computed:-1/0,value:-1/0};return A(n,function(n,u,i){var a=t?t.call(r,n,u,i):n;a>e.computed&&(e={value:n,computed:a})}),e.value},j.min=function(n,t,r){if(!t&&j.isArray(n)&&n[0]===+n[0]&&n.length<65535)return Math.min.apply(Math,n);if(!t&&j.isEmpty(n))return 1/0;var e={computed:1/0,value:1/0};return A(n,function(n,u,i){var a=t?t.call(r,n,u,i):n;a<e.computed&&(e={value:n,computed:a})}),e.value},j.shuffle=function(n){var t,r=0,e=[];return A(n,function(n){t=j.random(r++),e[r-1]=e[t],e[t]=n}),e},j.sample=function(n,t,r){return arguments.length<2||r?n[j.random(n.length-1)]:j.shuffle(n).slice(0,Math.max(0,t))};var k=function(n){return j.isFunction(n)?n:function(t){return t[n]}};j.sortBy=function(n,t,r){var e=k(t);return j.pluck(j.map(n,function(n,t,u){return{value:n,index:t,criteria:e.call(r,n,t,u)}}).sort(function(n,t){var r=n.criteria,e=t.criteria;if(r!==e){if(r>e||r===void 0)return 1;if(e>r||e===void 0)return-1}return n.index-t.index}),"value")};var F=function(n){return function(t,r,e){var u={},i=null==r?j.identity:k(r);return A(t,function(r,a){var o=i.call(e,r,a,t);n(u,o,r)}),u}};j.groupBy=F(function(n,t,r){(j.has(n,t)?n[t]:n[t]=[]).push(r)}),j.indexBy=F(function(n,t,r){n[t]=r}),j.countBy=F(function(n,t){j.has(n,t)?n[t]++:n[t]=1}),j.sortedIndex=function(n,t,r,e){r=null==r?j.identity:k(r);for(var u=r.call(e,t),i=0,a=n.length;a>i;){var o=i+a>>>1;r.call(e,n[o])<u?i=o+1:a=o}return i},j.toArray=function(n){return n?j.isArray(n)?o.call(n):n.length===+n.length?j.map(n,j.identity):j.values(n):[]},j.size=function(n){return null==n?0:n.length===+n.length?n.length:j.keys(n).length},j.first=j.head=j.take=function(n,t,r){return null==n?void 0:null==t||r?n[0]:o.call(n,0,t)},j.initial=function(n,t,r){return o.call(n,0,n.length-(null==t||r?1:t))},j.last=function(n,t,r){return null==n?void 0:null==t||r?n[n.length-1]:o.call(n,Math.max(n.length-t,0))},j.rest=j.tail=j.drop=function(n,t,r){return o.call(n,null==t||r?1:t)},j.compact=function(n){return j.filter(n,j.identity)};var M=function(n,t,r){return t&&j.every(n,j.isArray)?c.apply(r,n):(A(n,function(n){j.isArray(n)||j.isArguments(n)?t?a.apply(r,n):M(n,t,r):r.push(n)}),r)};j.flatten=function(n,t){return M(n,t,[])},j.without=function(n){return j.difference(n,o.call(arguments,1))},j.uniq=j.unique=function(n,t,r,e){j.isFunction(t)&&(e=r,r=t,t=!1);var u=r?j.map(n,r,e):n,i=[],a=[];return A(u,function(r,e){(t?e&&a[a.length-1]===r:j.contains(a,r))||(a.push(r),i.push(n[e]))}),i},j.union=function(){return j.uniq(j.flatten(arguments,!0))},j.intersection=function(n){var t=o.call(arguments,1);return j.filter(j.uniq(n),function(n){return j.every(t,function(t){return j.indexOf(t,n)>=0})})},j.difference=function(n){var t=c.apply(e,o.call(arguments,1));return j.filter(n,function(n){return!j.contains(t,n)})},j.zip=function(){for(var n=j.max(j.pluck(arguments,"length").concat(0)),t=new Array(n),r=0;n>r;r++)t[r]=j.pluck(arguments,""+r);return t},j.object=function(n,t){if(null==n)return{};for(var r={},e=0,u=n.length;u>e;e++)t?r[n[e]]=t[e]:r[n[e][0]]=n[e][1];return r},j.indexOf=function(n,t,r){if(null==n)return-1;var e=0,u=n.length;if(r){if("number"!=typeof r)return e=j.sortedIndex(n,t),n[e]===t?e:-1;e=0>r?Math.max(0,u+r):r}if(y&&n.indexOf===y)return n.indexOf(t,r);for(;u>e;e++)if(n[e]===t)return e;return-1},j.lastIndexOf=function(n,t,r){if(null==n)return-1;var e=null!=r;if(b&&n.lastIndexOf===b)return e?n.lastIndexOf(t,r):n.lastIndexOf(t);for(var u=e?r:n.length;u--;)if(n[u]===t)return u;return-1},j.range=function(n,t,r){arguments.length<=1&&(t=n||0,n=0),r=arguments[2]||1;for(var e=Math.max(Math.ceil((t-n)/r),0),u=0,i=new Array(e);e>u;)i[u++]=n,n+=r;return i};var R=function(){};j.bind=function(n,t){var r,e;if(_&&n.bind===_)return _.apply(n,o.call(arguments,1));if(!j.isFunction(n))throw new TypeError;return r=o.call(arguments,2),e=function(){if(!(this instanceof e))return n.apply(t,r.concat(o.call(arguments)));R.prototype=n.prototype;var u=new R;R.prototype=null;var i=n.apply(u,r.concat(o.call(arguments)));return Object(i)===i?i:u}},j.partial=function(n){var t=o.call(arguments,1);return function(){return n.apply(this,t.concat(o.call(arguments)))}},j.bindAll=function(n){var t=o.call(arguments,1);if(0===t.length)throw new Error("bindAll must be passed function names");return A(t,function(t){n[t]=j.bind(n[t],n)}),n},j.memoize=function(n,t){var r={};return t||(t=j.identity),function(){var e=t.apply(this,arguments);return j.has(r,e)?r[e]:r[e]=n.apply(this,arguments)}},j.delay=function(n,t){var r=o.call(arguments,2);return setTimeout(function(){return n.apply(null,r)},t)},j.defer=function(n){return j.delay.apply(j,[n,1].concat(o.call(arguments,1)))},j.throttle=function(n,t,r){var e,u,i,a=null,o=0;r||(r={});var c=function(){o=r.leading===!1?0:new Date,a=null,i=n.apply(e,u)};return function(){var l=new Date;o||r.leading!==!1||(o=l);var f=t-(l-o);return e=this,u=arguments,0>=f?(clearTimeout(a),a=null,o=l,i=n.apply(e,u)):a||r.trailing===!1||(a=setTimeout(c,f)),i}},j.debounce=function(n,t,r){var e,u,i,a,o;return function(){i=this,u=arguments,a=new Date;var c=function(){var l=new Date-a;t>l?e=setTimeout(c,t-l):(e=null,r||(o=n.apply(i,u)))},l=r&&!e;return e||(e=setTimeout(c,t)),l&&(o=n.apply(i,u)),o}},j.once=function(n){var t,r=!1;return function(){return r?t:(r=!0,t=n.apply(this,arguments),n=null,t)}},j.wrap=function(n,t){return function(){var r=[n];return a.apply(r,arguments),t.apply(this,r)}},j.compose=function(){var n=arguments;return function(){for(var t=arguments,r=n.length-1;r>=0;r--)t=[n[r].apply(this,t)];return t[0]}},j.after=function(n,t){return function(){return--n<1?t.apply(this,arguments):void 0}},j.keys=w||function(n){if(n!==Object(n))throw new TypeError("Invalid object");var t=[];for(var r in n)j.has(n,r)&&t.push(r);return t},j.values=function(n){for(var t=j.keys(n),r=t.length,e=new Array(r),u=0;r>u;u++)e[u]=n[t[u]];return e},j.pairs=function(n){for(var t=j.keys(n),r=t.length,e=new Array(r),u=0;r>u;u++)e[u]=[t[u],n[t[u]]];return e},j.invert=function(n){for(var t={},r=j.keys(n),e=0,u=r.length;u>e;e++)t[n[r[e]]]=r[e];return t},j.functions=j.methods=function(n){var t=[];for(var r in n)j.isFunction(n[r])&&t.push(r);return t.sort()},j.extend=function(n){return A(o.call(arguments,1),function(t){if(t)for(var r in t)n[r]=t[r]}),n},j.pick=function(n){var t={},r=c.apply(e,o.call(arguments,1));return A(r,function(r){r in n&&(t[r]=n[r])}),t},j.omit=function(n){var t={},r=c.apply(e,o.call(arguments,1));for(var u in n)j.contains(r,u)||(t[u]=n[u]);return t},j.defaults=function(n){return A(o.call(arguments,1),function(t){if(t)for(var r in t)n[r]===void 0&&(n[r]=t[r])}),n},j.clone=function(n){return j.isObject(n)?j.isArray(n)?n.slice():j.extend({},n):n},j.tap=function(n,t){return t(n),n};var S=function(n,t,r,e){if(n===t)return 0!==n||1/n==1/t;if(null==n||null==t)return n===t;n instanceof j&&(n=n._wrapped),t instanceof j&&(t=t._wrapped);var u=l.call(n);if(u!=l.call(t))return!1;switch(u){case"[object String]":return n==String(t);case"[object Number]":return n!=+n?t!=+t:0==n?1/n==1/t:n==+t;case"[object Date]":case"[object Boolean]":return+n==+t;case"[object RegExp]":return n.source==t.source&&n.global==t.global&&n.multiline==t.multiline&&n.ignoreCase==t.ignoreCase}if("object"!=typeof n||"object"!=typeof t)return!1;for(var i=r.length;i--;)if(r[i]==n)return e[i]==t;var a=n.constructor,o=t.constructor;if(a!==o&&!(j.isFunction(a)&&a instanceof a&&j.isFunction(o)&&o instanceof o))return!1;r.push(n),e.push(t);var c=0,f=!0;if("[object Array]"==u){if(c=n.length,f=c==t.length)for(;c--&&(f=S(n[c],t[c],r,e)););}else{for(var s in n)if(j.has(n,s)&&(c++,!(f=j.has(t,s)&&S(n[s],t[s],r,e))))break;if(f){for(s in t)if(j.has(t,s)&&!c--)break;f=!c}}return r.pop(),e.pop(),f};j.isEqual=function(n,t){return S(n,t,[],[])},j.isEmpty=function(n){if(null==n)return!0;if(j.isArray(n)||j.isString(n))return 0===n.length;for(var t in n)if(j.has(n,t))return!1;return!0},j.isElement=function(n){return!(!n||1!==n.nodeType)},j.isArray=x||function(n){return"[object Array]"==l.call(n)},j.isObject=function(n){return n===Object(n)},A(["Arguments","Function","String","Number","Date","RegExp"],function(n){j["is"+n]=function(t){return l.call(t)=="[object "+n+"]"}}),j.isArguments(arguments)||(j.isArguments=function(n){return!(!n||!j.has(n,"callee"))}),"function"!=typeof/./&&(j.isFunction=function(n){return"function"==typeof n}),j.isFinite=function(n){return isFinite(n)&&!isNaN(parseFloat(n))},j.isNaN=function(n){return j.isNumber(n)&&n!=+n},j.isBoolean=function(n){return n===!0||n===!1||"[object Boolean]"==l.call(n)},j.isNull=function(n){return null===n},j.isUndefined=function(n){return n===void 0},j.has=function(n,t){return f.call(n,t)},j.noConflict=function(){return n._=t,this},j.identity=function(n){return n},j.times=function(n,t,r){for(var e=Array(Math.max(0,n)),u=0;n>u;u++)e[u]=t.call(r,u);return e},j.random=function(n,t){return null==t&&(t=n,n=0),n+Math.floor(Math.random()*(t-n+1))};var I={escape:{"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#x27;"}};I.unescape=j.invert(I.escape);var T={escape:new RegExp("["+j.keys(I.escape).join("")+"]","g"),unescape:new RegExp("("+j.keys(I.unescape).join("|")+")","g")};j.each(["escape","unescape"],function(n){j[n]=function(t){return null==t?"":(""+t).replace(T[n],function(t){return I[n][t]})}}),j.result=function(n,t){if(null==n)return void 0;var r=n[t];return j.isFunction(r)?r.call(n):r},j.mixin=function(n){A(j.functions(n),function(t){var r=j[t]=n[t];j.prototype[t]=function(){var n=[this._wrapped];return a.apply(n,arguments),z.call(this,r.apply(j,n))}})};var N=0;j.uniqueId=function(n){var t=++N+"";return n?n+t:t},j.templateSettings={evaluate:/<%([\s\S]+?)%>/g,interpolate:/<%=([\s\S]+?)%>/g,escape:/<%-([\s\S]+?)%>/g};var q=/(.)^/,B={"'":"'","\\":"\\","\r":"r","\n":"n","        ":"t","\u2028":"u2028","\u2029":"u2029"},D=/\\|'|\r|\n|\t|\u2028|\u2029/g;j.template=function(n,t,r){var e;r=j.defaults({},r,j.templateSettings);var u=new RegExp([(r.escape||q).source,(r.interpolate||q).source,(r.evaluate||q).source].join("|")+"|$","g"),i=0,a="__p+='";n.replace(u,function(t,r,e,u,o){return a+=n.slice(i,o).replace(D,function(n){return"\\"+B[n]}),r&&(a+="'+\n((__t=("+r+"))==null?'':_.escape(__t))+\n'"),e&&(a+="'+\n((__t=("+e+"))==null?'':__t)+\n'"),u&&(a+="';\n"+u+"\n__p+='"),i=o+t.length,t}),a+="';\n",r.variable||(a="with(obj||{}){\n"+a+"}\n"),a="var __t,__p='',__j=Array.prototype.join,"+"print=function(){__p+=__j.call(arguments,'');};\n"+a+"return __p;\n";try{e=new Function(r.variable||"obj","_",a)}catch(o){throw o.source=a,o}if(t)return e(t,j);var c=function(n){return e.call(this,n,j)};return c.source="function("+(r.variable||"obj")+"){\n"+a+"}",c},j.chain=function(n){return j(n).chain()};var z=function(n){return this._chain?j(n).chain():n};j.mixin(j),A(["pop","push","reverse","shift","sort","splice","unshift"],function(n){var t=e[n];j.prototype[n]=function(){var r=this._wrapped;return t.apply(r,arguments),"shift"!=n&&"splice"!=n||0!==r.length||delete r[0],z.call(this,r)}}),A(["concat","join","slice"],function(n){var t=e[n];j.prototype[n]=function(){return z.call(this,t.apply(this._wrapped,arguments))}}),j.extend(j.prototype,{chain:function(){return this._chain=!0,this},value:function(){return this._wrapped}})}).call(this);
