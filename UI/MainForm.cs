﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using DataAccess;
using DataAccess.Entities;

namespace UI
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            DbUtility.CreateDatabase();
            dataGridView1.DataSource = new BindingList<Category>(DbUtility.GetAllCategories());
            dataGridView2.DataSource = new BindingList<BlacklistedShop>(DbUtility.GetAllBlacklisted());
        }

        private void SaveCategories()
        {
            var list = new List<Category>();
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (row.Cells["NameColumn"].Value != null && row.Cells["UrlColumn"].Value != null)
                    list.Add(new Category()
                    {
                        Id = Int32.Parse(row.Cells["IdColumn"].Value.ToString()),
                        Name = row.Cells["NameColumn"].Value.ToString(),
                        Url = row.Cells["UrlColumn"].Value.ToString()
                    });
            }
            DbUtility.SaveCategories(list);
        }

        private void SaveBlacklisted()
        {
            var list = new List<BlacklistedShop>();
            foreach (DataGridViewRow row in dataGridView2.Rows)
            {
                if (row.Cells["ShopIdColumn"].Value != null)
                    list.Add(new BlacklistedShop()
                    {
                        Id = Int32.Parse(row.Cells["IdColumn2"].Value.ToString()),
                        ShopId = row.Cells["ShopIdColumn"].Value.ToString()
                    });
            }
            DbUtility.SaveBlacklisted(list);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SaveCategories();
            SaveBlacklisted();
            dataGridView1.DataSource = new BindingList<Category>(DbUtility.GetAllCategories());
            dataGridView2.DataSource = new BindingList<BlacklistedShop>(DbUtility.GetAllBlacklisted());
            var form2 = new RunningForm();
            form2.Process(DbUtility.GetAllCategories(), trackBar1.Value);
            form2.ShowDialog();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            label2.Text = trackBar1.Value.ToString();
        }

        private async void button2_Click(object sender, EventArgs e)
        {
            await Export();
        }

        private async Task Export()
        {
            var form = new WaitForm();
            form.Show();
            await Task.Run(() =>
            {
                SaveBlacklisted();
                var folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Results");
                if (Directory.Exists(folder))
                    Directory.Delete(folder, true);
                Directory.CreateDirectory(folder);
                var categories = DbUtility.GetAllCategories();
                var blackListed = DbUtility.GetAllBlacklisted();
                foreach (var category in categories)
                {
                    var prices = DbUtility.GetMinimalPricesForCategotyProducts(category, blackListed);
                    foreach (var price in prices)
                    {
                        var saveData = string.Format("{0};{1};{2};{3};{4}",
                            Encoding.Default.GetString(Encoding.Convert(Encoding.Default,
                                Encoding.GetEncoding("windows-1251"),
                                Encoding.Default.GetBytes(category.Name))),
                            HttpUtility.HtmlDecode(price.ManufacturerName),
                            HttpUtility.HtmlDecode(new Regex(price.ManufacturerName).Replace(price.ProductName, "", 1).Trim()),
                            price.TotalPrice,
                            price.ShopUrl
                            );
                        File.AppendAllText(Path.Combine(folder, string.Format("{0}.scv", category.Name)), saveData + Environment.NewLine);
                        File.AppendAllText(Path.Combine(folder, "Result.scv"), saveData + Environment.NewLine);
                    }
                }

            });

            form.Close();
        }
    }
}
