﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataAccess;
using DataAccess.Entities;
using Interfaces;
using Interfaces.ParsingItems;
using Ninject;
using System.Threading;
using System.Threading.Tasks;
using NLog;

namespace UI
{
    public partial class RunningForm : Form
    {
        private const int LogMaxLines = 1000;
        private PauseTokenSource m_pauseTokeSource = null;
        private CancellationTokenSource m_cancelationTokenSource = null;
        private bool isPaused = false;
        private IKernel _kernel;
        private ConcurrentQueue<Category> _categories = new ConcurrentQueue<Category>();
        private int _executingTrhreads = 0;
        private object _locker = new object();
        private SemaphoreSlim abortingWaiter = new SemaphoreSlim(0, 1);
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public RunningForm()
        {
            InitializeComponent();
            _kernel = new StandardKernel();
            _kernel.Load("Parsing.Onliner.dll");
            _kernel.Load("SpySProxyProvider.dll"); 
            textBoxLogger.TextChanged+=TextBoxLoggerOnTextChanged;
        }

        private void TextBoxLoggerOnTextChanged(object sender, EventArgs eventArgs)
        {
            listBoxLogger.Invoke(new Action(() =>
            {
                var textbox = (TextBox) sender;
                listBoxLogger.Items.Insert(0, textbox.Text);

                while (listBoxLogger.Items.Count > LogMaxLines)
                {
                    listBoxLogger.Items.RemoveAt(listBoxLogger.Items.Count - 1);
                }
            }));
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            m_pauseTokeSource.IsPaused = !m_pauseTokeSource.IsPaused;
            button1.Text = m_pauseTokeSource.IsPaused ? "Продолжить" : "Пауза";
            if (m_pauseTokeSource.IsPaused)
                label2.Invoke(SetStatus("Пауза"));
            else
            {
                label2.Invoke(SetStatus("Выполняется"));
            }
        }

        private async void button2_Click_1(object sender, EventArgs e)
        {
            m_pauseTokeSource.IsPaused = false;
            m_cancelationTokenSource.Cancel();
            var form = new WaitForm();
            form.Show();
            m_cancelationTokenSource.Cancel();
            await abortingWaiter.WaitAsync();

            form.Close();
            this.Close();
        }
        
        public void Process(IEnumerable<Category> categories, int threads)
        {
            DbUtility.ClearPreviousData();
            m_pauseTokeSource = new PauseTokenSource();
            m_cancelationTokenSource = new CancellationTokenSource();
            _categories = new ConcurrentQueue<Category>();
            foreach (var category in categories)
            {
                _categories.Enqueue(category);
            }

            for (int i = 0; i < threads; i++)
            {
                _executingTrhreads++;
                var task = new Task(async () =>
                {
                    Category category;
                    while (_categories.TryDequeue(out category))
                    {
                        try
                        {
                            await ProcessCategory(category, m_pauseTokeSource.Token, m_cancelationTokenSource.Token);
                        }
                        catch (Exception ex )
                        {
                            Logger.Error(ex);
                        }
                    }
                    lock (_locker)
                    {
                        _executingTrhreads--;
                        if (_executingTrhreads == 0)
                        {
                            label2.Invoke(SetStatus("Готово"));
                            abortingWaiter.Release();
                        }
                    }
                });
                task.Start();
            }
        }

        private Delegate SetStatus(string p)
        {
            return new Action(()=>label2.Text = p);
        }

        public async Task ProcessCategory(Category category, PauseToken pauseToken, CancellationToken cancelToken)
        {
            var finder = _kernel.Get<IDataCollector>();
            var manufacturers = finder.GetAllManufacturersFromCategory(new CategoryItem(category.Name, category.Url));
            foreach (var manufacturerItem in manufacturers)
            {
                await pauseToken.WaitWhilePausedAsync();
                if (cancelToken.IsCancellationRequested) return;
                var manufacturer = new Manufacturer()
                {
                    CategoryId = category.Id,
                    Name = manufacturerItem.Name,
                    Url = manufacturerItem.Url
                };
                DbUtility.Save(manufacturer);
                var products = finder.GetAllProductsFromManufacturer(manufacturerItem);
                foreach (var productItem in products)
                {
                    await pauseToken.WaitWhilePausedAsync();
                    if (cancelToken.IsCancellationRequested) return;
                    var product = new Product()
                    {
                        CategoryId = category.Id,
                        Name = productItem.Name,
                        Url = productItem.Url,
                        ManufacturerId = manufacturer.Id,
                        IsAvailable = productItem.IsAvailable
                    };
                    DbUtility.Save(product);
                    if (productItem.IsAvailable)
                    {
                        var prices = finder.GetAllPricesItemsFromProduct(productItem, Currency.USD);
                        foreach (var priceItem in prices)
                        {
                            DbUtility.Save(new Price()
                            {
                                CategoryId = category.Id,
                                ManufacturerId = manufacturer.Id,
                                ManufacturerName = manufacturerItem.Name,
                                Currency = (int)Currency.USD,
                                ProductId = product.Id,
                                ProductName = productItem.Name,
                                IsClearingPossible = priceItem.IsClearingPossible,
                                IsCreditPossible = priceItem.IsCreditPossible,
                                IsInStock = priceItem.IsInStock,
                                ShopUrl = priceItem.ShopUrl,
                                TotalPrice = priceItem.Price
                            });

                        }
                    }
                }
            }

        }

        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

    }
}
